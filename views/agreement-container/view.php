<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAddressContainer */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Agreement Address Containers', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agreement-address-container-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'agreement_address_id',
            'agreement_number',
            'agreement_description',
            'waste_collection_address',
            'waste_container_address',
            'building_address',
            'waste_container_number',
            'waste_container_type',
            'waste_container_count',
            'client_name',
        ],
    ]) ?>

</div>
