<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('admin','Agreement Address Containers');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-address-container-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
//            'agreement_address_id',
            'agreement_number',
            'agreement_description',
            'waste_collection_address',
            'waste_container_address',
            'building_address',
            'waste_container_number',
            'waste_container_type',
            'waste_container_count',
            'client_name',
        ],
    ]); ?>


</div>
