<?php

/**
 * Users creation view
 *
 * @var yii\web\View $this
 * @var app\models\User $model
 */
?><div class="admin-page admin-page_data-create user-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
