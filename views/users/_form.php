<?php

use app\models\User;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * Form create/update form view
 *
 * @var yii\web\View $this
 * @var app\models\User $model
 */
?><div class="admin-data-form user-form">

    <?php $form = ActiveForm::begin(); ?>
    <header class="admin-data-form__header row">
        <?php if ($model->isNewRecord) { ?>
            <div class="col-md-offset-6 col-md-1">
                <?= Html::a(
                    Yii::t('app', 'Cancel'),
                    ['index'],
                    ['class' => 'btn btn-md btn-block btn-default']
                ) ?>
            </div>
            <div class="col-md-3">
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Save & create new'),
                    [
                        'class' => 'btn btn-md btn-block btn-info',
                        'name' => 'mode',
                        'value' => 'save-and-create-new'
                    ]
                ) ?>
            </div>
            <div class="col-md-2">
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Save'),
                    ['class' => 'btn btn-md btn-block btn-success']
                ) ?>
            </div>
        <?php } else { ?>
            <div class="col-md-2">
                <?= Html::a(
                    '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Create new'),
                    ['create'],
                    ['class' => 'btn btn-md btn-block btn-success']
                ) ?>
            </div>
            <div class="col-md-offset-3 col-md-1">
                <?= Html::a(
                    Yii::t('app', 'Delete'),
                    ['delete', 'id' => $model->id],
                    [
                        'class' => 'btn btn-link',
                        'data' => [
                            'confirm' => Yii::t('app', 'Are you sure you want to delete this {modelClass}?', [
                                'modelClass' => Yii::t('app.models.User', 'User'),
                            ]),
                            'method' => 'post',
                        ],
                    ]
                ) ?>
            </div>
            <div class="col-md-1">
                <?= Html::a(
                    Yii::t('app', 'Cancel'),
                    ['index'],
                    ['class' => 'btn btn-md btn-block btn-default']
                ) ?>
            </div>
            <div class="col-md-3">
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Save & create new'),
                    [
                        'class' => 'btn btn-md btn-block btn-info',
                        'name' => 'mode',
                        'value' => 'save-and-create-new'
                    ]
                ) ?>
            </div>
            <div class="col-md-2">
                <?= Html::submitButton(
                    '<span class="glyphicon glyphicon-floppy-disk"></span> ' . Yii::t('app', 'Save'),
                    ['class' => 'btn btn-md btn-block btn-primary']
                ) ?>
            </div>
        <?php } ?>
    </header>
    <div class="row">
        <div class="col-sm-offset-2 col-sm-6">
            <div class="form-horizontal">
                <?= $form->field(
                    $model,
                    'username',
                    [
                        'labelOptions' => ['class' => 'col-sm-6 control-label'],
                        'template' => "{label}<div class=\"col-sm-6\">{input}\n{hint}\n{error}</div>"
                    ]
                )->textInput(
                    [
                        'maxlength' => true,
                        'class' => 'form-control',
                        'placeholder' => Yii::t('app', 'email.address@example.com'),
                    ]
                ) ?>
                <?= $form->field(
                    $model,
                    'password',
                    [
                        'labelOptions' => ['class' => 'col-sm-6 control-label'],
                        'template' => "{label}<div class=\"col-sm-6\">{input}\n{hint}\n{error}</div>"
                    ]
                )->passwordInput(
                    [
                        'maxlength' => true,
                        'class' => 'form-control',
                    ]
                ) ?>
                <?= $form->field(
                    $model,
                    'status',
                    [
                        'labelOptions' => ['class' => 'col-sm-6 control-label'],
                        'template' => "{label}<div class=\"col-sm-6\">{input}\n{hint}\n{error}</div>"
                    ]
                )->dropDownList(
                    $model->statuses,
                    [
                        'class'=>'form-control'
                    ]
                ) ?>
            </div>
        </div>
    </div>
    <footer class="admin-data-form__footer">
    <?php if (!$model->isNewRecord): ?>
        <span class="entry__created"><?= Yii::t('app', 'Created') ?>: <span class="created-by"><?= User::usernameById($model->created_by) ?></span> (<span class="created-at"><?= $model->formattedCreatedAt ?></span>)</span>
        <?php if ($model->updated_at): ?>
        <span class="entry__updated"><?= Yii::t('app', 'Updated') ?>: <span class="updated-by"><?= User::usernameById($model->updated_by) ?></span> (<span class="updated-at"><?= $model->formattedUpdatedAt ?></span>)</span>
        <?php endif; ?>
    <?php endif; ?>
    </footer>
    <?php ActiveForm::end(); ?>

</div>
