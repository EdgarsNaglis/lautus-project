<?php

use app\models\User;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * Users list view
 *
 * @var yii\web\View $this
 * @var app\models\search\User $searchModel
 * @var yii\data\ActiveDataProvider $dataProvider
 */
?><div class="admin-page admin-page_data-index user-index">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="row">
        <div class="col-sm-4 col-sm-offset-8 col-md-2 col-md-offset-10"><?=  Html::a(
                '<span class="glyphicon glyphicon-plus"></span> ' . Yii::t('app', 'Create new'),
                ['create'],
                [
                    'class' => 'btn btn-success btn-block btn-sm',
                    'style' => 'margin-bottom: 10px;'
                ]
            )
            ?></div>
    </div>
<?php Pjax::begin(); ?>    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'id',
                'headerOptions' => ['class' => 'col_id'],
                'contentOptions' => ['class' => 'col_id'],
            ],
            [
                'attribute' => 'username',
                'headerOptions' => ['class' => 'col_username'],
                'contentOptions' => ['class' => 'col_username'],
                'format' => 'html',
                'value' => function($searchModel) {

                    /**
                     * @var app\models\search\User $searchModel
                     */
                    return '<a title="' . Yii::t('yii', 'View') . '" href="' .
                        Url::to(['users/view', 'id' => $searchModel->id]) .
                    '">' . $searchModel->username . '</a>';
                },
            ],
            [
                'attribute' => 'status',
                'headerOptions' => ['class' => 'col_status'],
                'contentOptions' => ['class' => 'col_status'],
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'status',
                    ArrayHelper::merge(
                        [
                            0 => Yii::t('app', 'Any')
                        ],
                        $searchModel->statuses
                    ),
                    ['class'=>'form-control']
                ),
                'value' => function($searchModel) {

                    /**
                     * @var app\models\search\User $searchModel
                     */
                    return $searchModel->statusTitle;
                },
            ],
            [
                'attribute' => 'updated_at',
                'headerOptions' => ['class' => 'col_updated-at'],
                'contentOptions' => ['class' => 'col_updated-at'],
                'label' => Yii::t('app', 'Updated'),
                'format' => 'html',
                'filter' => \yii\jui\DatePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'updated_at',
                    'language' => 'lv',
                    'dateFormat' => 'yyyy-MM-dd',
                    'options' => ['class' => 'form-control']
                ]),
                'value' => function($searchModel) {

                    /**
                     * @var app\models\search\User $searchModel
                     */
                    return (!is_null($searchModel->updated_by) ?
                        User::usernameById($searchModel->updated_by) .
                            '<span class="col_updated-by__value">' .
                            $searchModel->formattedUpdatedAt .
                            '</span>' :
                        '-'
                    );
                },
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'headerOptions' => ['class' => 'col_actions'],
                'buttonOptions' => ['class' => 'btn btn-link btn-sm'],
                'template' => '<div class="btn-group" role="group">{view}{update}{delete}</div>'
            ],
        ],
    ]); ?>
<?php Pjax::end(); ?></div>
