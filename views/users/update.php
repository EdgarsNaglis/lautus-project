<?php

/**
 * Users update view
 *
 * @var yii\web\View $this
 * @var app\models\User $model
 */
?><div class="admin-page admin-page_data-update user-update">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
