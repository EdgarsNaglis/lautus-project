<?php

use app\models\User;
use yii\helpers\Html;
use yii\widgets\DetailView;

/**
 * Users view
 *
 * @var yii\web\View $this
 * @var app\models\User $model
 */
?><div class="admin-page admin-page_data-view user-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            [
                'attribute' => 'status',
                'value' => $model->statusTitle
            ]
        ],
    ]) ?>
    <footer class="admin-data-view__footer row">
        <div class="col-md-9 entry-info">
        <?php if (!$model->isNewRecord): ?>
            <span class="entry__created"><?= Yii::t('app', 'Created') ?>: <span class="created-by"><?= User::usernameById($model->created_by) ?></span> (<span class="created-at"><?= $model->formattedCreatedAt ?></span>)</span>
            <?php if ($model->updated_at): ?>
            <span class="entry__updated"><?= Yii::t('app', 'Updated') ?>: <span class="updated-by"><?= User::usernameById($model->updated_by) ?></span> (<span class="updated-at"><?= $model->formattedUpdatedAt ?></span>)</span>
            <?php endif; ?>
        <?php endif; ?>
        </div>
        <div class="col-md-1">
            <?= Html::a(Yii::t('app', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-block btn-link',
                'data' => [
                    'confirm' => Yii::t('app', 'Are you sure you want to delete this {modelClass}?', [
                        'modelClass' => Yii::t('app.models.User', 'User'),
                    ]),
                    'method' => 'post',
                ],
            ]) ?>
        </div>
        <div class="col-md-2">
            <?= Html::a(Yii::t('app', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-block btn-primary']) ?>
        </div>
    </footer>
</div>
