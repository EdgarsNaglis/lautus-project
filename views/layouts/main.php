<?php

/* @var $this \yii\web\View */

/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\assets\FontawesomeAsset;

AppAsset::register($this);
?>


<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);

    $menuItems = [];
    if (Yii::$app->user->isGuest) {
        $menuItems[] =
            [
                'label' => Yii::t('admin', 'Login'),
                'url' => ['/site/login']
            ];
    } else {
        $menuItems[] =
            [
                'label' => Yii::t('common', 'Import'),
                'url' => ['/import-data']
            ];
        $menuItems[] =
            [
                'label' => Yii::t('admin', 'Export (action)'),
                'url' => ['/export-data']
            ];
        $menuItems[] =
            [
                'label' => Yii::t('common', 'Agreement'),
                'url' => ['/data/address']
            ];
        $menuItems[] =
            [
                'label' => Yii::t('common', 'Containers'),
                'url' => ['/data/container']
            ];

        $menuItems[] =
            [
                'label' => Html::beginForm(['/site/logout'], 'post')
                    . Html::submitButton(
                        Yii::t('admin', 'Logout') . ' (' . Yii::$app->user->identity->username . ')',
                        [
                            'class' => 'btn btn-link logout',
                            'style' => 'padding: 0; border: none;'
                        ]
                    )
                    . Html::endForm(),
                'url' => ['/import-data']
            ];
    }

    if (!empty($menuItems)) {
        echo Nav::widget([
            'encodeLabels' => false,
            'activateParents' => true,
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menuItems,
        ]);
    }
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Lautus.lv pakalpojumu līgumu konvertācijas sistēma <?= date(2020) ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
