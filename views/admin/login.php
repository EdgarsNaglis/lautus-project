<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var yii\web\View $this
 * @var app\models\LoginForm $model
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 */
$this->registerJs(<<<JS
$('#loginform-username').focus();
JS
)
?><div class="admin-login">
    <?php $form = ActiveForm::begin([
        'id' => 'login-form'
    ]); ?>
    <div class="row">
        <div class="col-sm-6 col-sm-offset-3 col-md-4 col-md-offset-4">
            <?= $form->field($model, 'username')->textInput(['placeholder' => Yii::t('app.models.User', 'Username')])->label(false) ?>
            <?= $form->field($model, 'password')->label(false)->passwordInput(['placeholder' => Yii::t('app.models.User', 'Password')]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-3 col-sm-offset-3 col-md-2 col-md-offset-4">
            <div class="checkbox">
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
            </div>
        </div>
        <div class="col-sm-3 col-md-2">
            <div class="form-group">
                <?= Html::submitButton(
                    '<i class="fa fa-fw fa-sign-in"></i> ' . Yii::t('app', 'Login'),
                    [
                        'class' => 'btn btn-success btn-block',
                        'name' => 'login-button'
                    ]
                ) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
