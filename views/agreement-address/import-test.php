<?php
/**
 * @see \app\controllers\AgreementAddressController::actionImport()
 */

use app\models\AgreementDataImportFormTest;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var AgreementDataImportFormTest $model
 * @var yii\data\DataProviderInterface|null $addressesData
 * @var yii\data\DataProviderInterface|null $containersData
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\AgreementContainerController
 */

?>

<div class="agreement-address-import-test">

    <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

    <?= $form->field($model, 'import_file')->fileInput() ?>
    <?= $form->field($model, 'mode')->radioList([
        $model::MODE_SIMULATE => Yii::t('admin','Simulate import, show as table'),
        $model::MODE_WRITE => Yii::t('admin','Import in DB'),
    ]) ?>

    <?= $form->field($model, 'sheet_type')->radioList([
        $model::SHEET_ONE => Yii::t('admin','One'),
        $model::SHEET_TWO => Yii::t('admin','Two'),
        $model::SHEET_THREE => Yii::t('admin','Three'),
    ]) ?>

    <?= $form->field($model, 'sheet_number')->textInput(['type' => 'number', 'value' => 1, 'min' => '1', 'max' => '100']) ?>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('admin','Import'), ['class' => 'btn btn-success']) ?>
    </div>

    <i class="fa fa-spinner" aria-hidden="true"></i>

    <?php ActiveForm::end(); ?>

</div>
