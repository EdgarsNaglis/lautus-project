<?php
/**
 * @see \app\controllers\AgreementAddressController::actionImport()
 */

use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\DataProviderInterface $addressesData
 * @var yii\data\DataProviderInterface $containersData
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\AgreementContainerController
 */

?>

<div class="agreement-address-container">
    <?= GridView::widget([
        'dataProvider' => $containersData,
        'columns' => [
            'id',
            'agreement_number',
            'agreement_description',
            'waste_collection_address',
            'waste_container_address',
            'building_address',
            'waste_container_type',
            'waste_container_number',
            'waste_container_count',
            'company',
            'client_name',
        ],
    ]); ?>
</div>
