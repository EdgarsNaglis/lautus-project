<?php
/**
 * @see \app\controllers\AgreementAddressController::actionImport()
 */

use yii\grid\GridView;

/**
 * @var yii\web\View $this
 * @var yii\data\DataProviderInterface $addressesData
 * @var yii\data\DataProviderInterface $containersData
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\AgreementContainerController
 */

?>

<div class="agreement-address">
    <?= GridView::widget([
        'dataProvider' => $addressesData,
        'columns' => [
            'id',
            'agreement_number',
            'agreement_date',
            'waste_collection_start_date',
            'waste_collection_end_date',
            'registration_code',
            'client_code',
            'company_email:email',
            'client_email:email',
            'waste_collection_address',
            'waste_container_address',
            'building_address',
            'waste_collection_frequency',
        ],
    ]); ?>
</div>
