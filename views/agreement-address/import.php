<?php
/**
 * @see \app\controllers\AgreementAddressController::actionImport()
 */

use app\assets\AppAsset;
use app\models\AgreementDataImportForm;
use yii\bootstrap\Tabs;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var AgreementDataImportForm $model
 * @var yii\data\DataProviderInterface|null $addressesData
 * @var yii\data\DataProviderInterface|null $containersData
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\AgreementAddressController
 */

$this->registerJs(<<<JS
$('#agreement-address-import').on('beforeSubmit', function () {
  $(".loader_agreement-address-import").show();
});
JS
    , $this::POS_READY
);
?>

<div class="agreement-address-import" style="margin-top: 30px">

    <?php $form = ActiveForm::begin([
            'id' => 'agreement-address-import',
            'options' => ['enctype' => 'multipart/form-data']
    ]) ?>

    <?= $form->field($model, 'import_file')->fileInput() ?>
    <?= $form->field($model, 'mode')->radioList([
        $model::MODE_SIMULATE => Yii::t('admin', 'Simulate import, show as table'),
        $model::MODE_WRITE => Yii::t('admin', 'Import in DB'),
    ]) ?>

    <?= $form->field($model, 'sheet_type')->radioList([
        $model::SHEET_AGREEMENT_ADDRESS => Yii::t('admin', 'Agreement Address'),
        $model::SHEET_AGREEMENT_ADDRESS_CONTAINER => Yii::t('admin', 'Agreement Address Containers'),
    ]) ?>

    <?= $form->field($model, 'sheet_number')->textInput([
        'type' => 'number',
        'value' => 1,
        'min' => '1',
        'max' => '100'
    ]) ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('admin', 'Import'), ['class' => 'button btn btn-success']) ?>
    </div>

    <div class="loader loader_agreement-address-import text-center" style="display: none;">
        <div><i class="fa fa-spinner fa-spin fa-3x" aria-hidden="true"></i></div>
        <h3><?= Yii::t(
                'admin',
                'Loading data'
            ) ?></h3>
    </div>

    <?php ActiveForm::end(); ?>

    <?php if ($addressesData !== null): ?>
        <div class="agreement-address">
            <?= GridView::widget([
                'dataProvider' => $addressesData,
                'columns' => [
                    'id',
                    'agreement_number',
                    'agreement_date',
                    'waste_collection_start_date',
                    'waste_collection_end_date',
                    'registration_code',
                    'client_code',
                    'company_email:email',
                    'client_email:email',
                    'waste_collection_address',
                    'waste_container_address',
                    'building_address',
                    'waste_collection_frequency',
                ],
            ]); ?>
        </div>
    <?php endif; ?>

    <?php if ($containersData !== null): ?>
        <div class="agreement-address-container">
            <?= GridView::widget([
                'dataProvider' => $containersData,
                'columns' => [
                    'id',
                    'agreement_number',
                    'agreement_description',
                    'waste_collection_address',
                    'waste_container_address',
                    'building_address',
                    'waste_container_type',
                    'waste_container_number',
                    'waste_container_count',
                    'company',
                    'client_name',
                ],
            ]); ?>
        </div>
    <?php endif; ?>

</div>
