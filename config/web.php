<?php

$params = require __DIR__ . '/params.php';
$db = require __DIR__ . '/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'asdfasdfasdfasdfasdfhgjghjhljlghjl',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\base\WebUserIdentity',
//            'enableAutoLogin' => true,
            'loginUrl' => ['site/login'],
        ],
        'session' => [
            'class' => 'yii\web\DbSession',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
               '/import-data' => '/agreement-address/import',
               '/export-data' => '/agreement-address/export',
               '/data/address' => '/agreement-address/index',
               '/data/container' => '/agreement-container/index',
            ],
        ],
        'i18n' => [
            'translations' => [
                'app*' => [
                    'class' => 'yii\i18n\PhpMessageSource'
                ],
                'admin' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'app.models.AgreementAddress' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'app.models.AgreementAddressContainer' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'app.models.AgreementDataImportForm' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
                'common' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                ],
            ],
        ],
    ],
    'params' => $params,
    'name' => 'Lautus.lv līgumu konvertācijas sistēma',
    'language' => 'lv-LV',
    'homeUrl' => '/import-data'
];

if (isset($params['site']['language'])) {
    $config['language'] = $params['site']['language'];
}

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*', '127.0.0.1', '::1', '192.168.83.*'],
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        // uncomment the following to add your IP if you are not connecting from localhost.
        'allowedIPs' => ['*', '127.0.0.1', '::1', '192.168.83.*'],
    ];
}

return $config;
