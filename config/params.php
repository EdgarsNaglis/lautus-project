<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',

    'allowedDeveloperIPs' => [
        '127.0.0.1',
        '::1',
        '192.168.56.*', // developer access when hosted in VirtualBox
    ],
];
