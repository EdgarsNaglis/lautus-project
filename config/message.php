<?php
/**
 * Configuration file for 'yii message/extract' command.
 */
return [
    'format' => 'db',
    'db' => 'db',
    'sourcePath' => '@app',
    'languages' => ['lv'],
    'translator' => 'Yii::t',
    'overwrite' => true,
    'removeUnused' => false,
    'markUnused' => true,
    'except' => [
        '.svn',
        '.git',
        '.gitignore',
        '.gitkeep',
        '.hgignore',
        '.hgkeep',
        '/messages',
        '/BaseYii.php',
    ],
    'only' => [
        '*.php',
    ],
    'ignoreCategories' => [
        'yii',
    ],
];
