<?php
namespace app\models;

use app\base\DateFormatter;
use Yii;
use yii\base\InvalidArgumentException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "{{%user}}".
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $status
 * @property string $last_login_at
 * @property string $created_at
 * @property integer $created_by
 * @property string $updated_at
 * @property integer $updated_by
 *
 * @property-read array $formattedCreatedAt see {@link User::getFormattedCreatedAt()} for more info
 * @property-read array $formattedUpdatedAt see {@link User::getFormattedUpdatedAt()} for more info
 * @property-read array $statuses see {@link User::getStatuses()} for more info
 * @property-read string $statusTitle see {@link User::getStatusTitle()} for more info
 * @property-read bool $hasIsAdminStatus see {@link User::getHasIsAdminStatus()} for more info
 *
 * @property User $createdBy
 * @property User $updatedBy
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\models\User
 */
class User extends ActiveRecord
{
    const STATUS_IS_ADMIN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'required'],
            ['password', 'passwordHashShouldBeSetValidator', 'on' => 'create'],
            ['username', 'string', 'max' => 100],
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'string'],
            ['username', 'unique', 'message' => Yii::t('app.models.User', 'This username has already been taken.')],
            [['password_hash'], 'string', 'max' => 100],
            ['status', 'default', 'value' => self::STATUS_IS_ADMIN],
            ['status', 'in', 'range' => [self::STATUS_IS_ADMIN]],
            ['password', 'safe'],
        ];
    }

    /**
     * @param $attribute
     */
    public function passwordHashShouldBeSetValidator($attribute)
    {
        if (is_null($this->password_hash)) {
            $this->addError($attribute, Yii::t('yii', '{attribute} cannot be blank.', ['attribute' => $attribute]));
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => Yii::t('app.models.User', 'User ID'),
            'username'      => Yii::t('app.models.User', 'Username'),
            'password_hash' => Yii::t('app.models.User', 'Password hash'),
            'status'        => Yii::t('app.models.User', 'Status'),
            'password'      => Yii::t('app.models.User', 'Password'),
            'last_login_at' => Yii::t('app.models.User', 'Last login at'),
            'created_at'    => Yii::t('app', 'Created at'),
            'created_by'    => Yii::t('app', 'Creator user ID'),
            'updated_at'    => Yii::t('app', 'Updated at'),
            'updated_by'    => Yii::t('app', 'Updater user ID'),
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::class, ['id' => 'updated_by']);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'Blameable' => [
                'class' => BlameableBehavior::class,
            ],
            'Timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('UTC_TIMESTAMP()'),
            ]
        ];
    }

    /**
     * @param string $password
     *
     * @return string
     */
    public static function generatePasswordHash(string $password)
    {
        return Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Changes password to new one
     *
     * @param string $password
     *
     * @param bool $saveAfterwards
     *
     * @return bool
     */
    public function setPassword(string $password, $saveAfterwards = false)
    {
        Yii::info('User ' . $this->id . ' password changed');
        $this->password_hash = self::generatePasswordHash($password);
        $this->password = '';
        if ($saveAfterwards) {
            return $this->save(false, ['password']);
        }

        return true;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return '';
    }

    /**
     * @param string $password
     *
     * @return bool
     */
    public function validatePassword(string $password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Returns possible values of "status" attribute along with value titles
     *
     * @return array
     */
    public static function getStatuses()
    {
        return [
            static::STATUS_IS_ADMIN => Yii::t('app.models.User', 'Is admin')
        ];
    }

    /**
     * Returns current "status" attribute value title
     *
     * @return string|null
     */
    public function getStatusTitle()
    {
        $statuses = $this->getStatuses();

        return (isset($statuses[$this->status]) ? $statuses[$this->status] : null);
    }

    /**
     * Returns "created at" value as formatted date string or as an DateTime object
     *
     * @param string|null $format
     *
     * @return string|\DateTime If format is null, DateTime object is returned.
     * @link http://php.net/manual/en/datetime.format.php
     */
    public function getFormattedCreatedAt($format = 'c')
    {
        return DateFormatter::formatUtcDate($this->created_at, $format);
    }

    /**
     * Returns "updated at" value as formatted date string or as an DateTime object
     *
     * @param string|null $format
     *
     * @return string|\DateTime If format is null, DateTime object is returned.
     * @link http://php.net/manual/en/datetime.format.php
     */
    public function getFormattedUpdatedAt($format = 'c')
    {
        return DateFormatter::formatUtcDate($this->updated_at, $format);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return null|User
     */
    public static function findByUsername($username)
    {
        return self::findOne(['username' => $username]);
    }

    /**
     * @return bool
     */
    public function getHasIsAdminStatus()
    {
        return ($this->status == self::STATUS_IS_ADMIN);
    }

    /**
     * @param $status
     *
     * @return int
     */
    public static function countUsersWithStatus($status)
    {
        $statuses = self::getStatuses();
        if (isset($statuses[$status])) {
            return User::find()->where(['status' => $status])->count();
        } else {
            Yii::error('Unknown status: ' . VarDumper::dumpAsString($status));
            throw new InvalidArgumentException('Unknown status');
        }
    }

    /**
     * @param $id
     *
     * @return mixed|string
     */
    public static function usernameById($id)
    {
        $username = self::find()
            ->where(['id' => $id])
            ->select(['username'])
            ->asArray()
            ->column();
        $username = (isset($username[0]) ? $username[0] : null);

        return $username;
    }
}
