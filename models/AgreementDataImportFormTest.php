<?php


namespace app\models;

use Closure;
use DateTime;
use DateTimeZone;
use PHPExcel;
use PHPExcel_Exception;
use PHPExcel_IOFactory;
use PHPExcel_Reader_Exception;
use PHPExcel_Worksheet;
use Yii;
use yii\base\Model;
use yii\data\ArrayDataProvider;
use yii\db\Exception;
use yii\helpers\VarDumper;
use yii\web\UploadedFile;

/**
 *
 * @property-read bool $isSimulateMode
 * @property-read int $importedCount
 * @property-read int $usersSpreadsheetNumber
 * @property-read bool $isSheetAgreementAddress
 * @property-read bool $isSheetAgreementAddressContainer
 * @property-read bool $isSheetNumberThree
 * @property-read bool $isSheetNumberTwo
 * @property-read bool $isSheetNumberOne
 * @property-read bool $isWriteMode
 */
class AgreementDataImportFormTest extends Model
{
    const MODE_SIMULATE = 'simulate';
    const MODE_WRITE = 'write';

    const SHEET_ONE = 'sheet_one';
    const SHEET_TWO = 'sheet_two';
    const SHEET_THREE = 'sheet_three';

    public $import_file;
    public $mode;
    public $sheet_type;
    public $sheet_number;

    /**
     * @var UploadedFile
     */
    public $importFile;
    /**
     * @var yii\data\DataProviderInterface
     */
    public $addressesData;
    /**
     * @var yii\data\DataProviderInterface
     */
    public $containersData;

    public function rules()
    {
        return [
            [['mode', 'sheet_type', 'sheet_number', 'import_file'], 'required'],
            [['importFile'], 'file', 'skipOnEmpty' => false/*, 'extensions' => 'csv'*/],
            [['mode'], 'in', 'range' => [static::MODE_SIMULATE, static::MODE_WRITE]],
            [
                ['sheet_type'],
                'in',
                'range' => [static::SHEET_ONE, static::SHEET_TWO, static::SHEET_THREE]
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'import_file' => Yii::t('app.models.AgreementDataImportForm', 'Import file'),
            'mode' => Yii::t('app.models.AgreementDataImportForm', 'Mode'),
            'sheet_type' => Yii::t('app.models.AgreementDataImportForm', 'Sheet type'),
            'sheet_number' => Yii::t('app.models.AgreementDataImportForm', 'Sheet number'),
        ];
    }


    public function init()
    {
        parent::init();
        $this->mode = static::MODE_SIMULATE;
        $this->sheet_type = static::SHEET_ONE;
    }

    public function getIsSimulateMode()
    {
        return $this->mode === static::MODE_SIMULATE;
    }

    public function getIsWriteMode()
    {
        return $this->mode === static::MODE_WRITE;
    }

    public function getIsSheetNumberOne()
    {
        return $this->sheet_type === static::SHEET_ONE;
    }

    public function getIsSheetNumberTwo()
    {
        return $this->sheet_type === static::SHEET_TWO;
    }

    public function getIsSheetNumberThree()
    {
        return $this->sheet_type === static::SHEET_THREE;
    }

    public function getUsersSpreadsheetNumber()
    {
        return $this->sheet_number - 1;
    }


    public function import()
    {
        if ($this->validate()) {
            return $this->doImport();
        }

        return false;
    }

    protected function doImport()
    {
        set_time_limit(0);

        Yii::beginProfile('Import action');
        $success = true;
//        $spreadsheetFilePath = Yii::getAlias('@app') . '/web/uploads/agreement-address-container-data.xlsx';
//        $spreadsheetFilePath = Yii::getAlias('@app') . '/web/uploads/agreement-address_3.xlsx';
        $spreadsheetFilePath = $this->importFile->tempName;
        if (!file_exists($spreadsheetFilePath)) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could not find spreadsheet file: ' . $spreadsheetFilePath)
            );
            $success = false;
        }

        // Truncate address table
        if ($this->getIsWriteMode() && $this->getIsSheetNumberOne()) {
            Yii::beginProfile('Clearing database data');
            if (!$this->clearOne()) {
                $success = false;
            }
            Yii::endProfile('Clearing database data');
        }

        // Truncate containers table
        if ($this->getIsWriteMode() && $this->getIsSheetNumberTwo()) {
            Yii::beginProfile('Clearing database data');
            if (!$this->clearTwo()) {
                $success = false;
            }
            Yii::endProfile('Clearing database data');
        }

        // Truncate containers table
        if ($this->getIsWriteMode() && $this->getIsSheetNumberThree()) {
            Yii::beginProfile('Clearing database data');
            if (!$this->clearThree()) {
                $success = false;
            }
            Yii::endProfile('Clearing database data');
        }

        if ($success) {
            $this->addressesData = new ArrayDataProvider([
                'key' => 'id',
                'sort' => [
                    'attributes' => array_keys((new AgreementAddress())->attributeLabels()),
                ],
                'pagination' => [
                    'pageSize' => 0,
                ],
                'allModels' => [],
                'modelClass' => AgreementAddress::class,
            ]);

            $this->containersData = new ArrayDataProvider([
                'key' => 'id',
                'sort' => [
                    'attributes' => array_keys((new AgreementAddressContainer())->attributeLabels()),
                ],
                'pagination' => [
                    'pageSize' => 0,
                ],
                'allModels' => [],
                'modelClass' => AgreementAddressContainer::class,
            ]);

            try {
                if ($this->getIsSheetNumberOne()) {
                    Yii::beginProfile('Extract addresses data from file');
                    $this->addressesData->allModels = $this->extractSheetOneDataFromFile(
                        $spreadsheetFilePath,
                        $this->getIsWriteMode(),
                        $this->getUsersSpreadsheetNumber()
                    );
                    Yii::endProfile('Extract addresses data from file');
                }

                if ($this->getIsSheetNumberTwo()) {
                    Yii::beginProfile('Extract containers data from file');
                    $this->containersData->allModels = $this->extractSheetTwoDataFromFile(
                        $spreadsheetFilePath,
                        $this->getIsWriteMode(),
                        $this->getUsersSpreadsheetNumber()
                    );
                    Yii::endProfile('Extract containers data from file');
                }
            } catch (PHPExcel_Exception $e) {
                Yii::error(
                    Yii::t('app.models.AgreementDataImportForm',
                        'Could not finish loading spreadsheet file: ' . $e->getMessage())
                );
                $success = false;
            }

        }
        Yii::endProfile('Import action');
        return $success;
    }

    /**
     * @return int
     */
    public function getImportedCount()
    {
        $count = 0;
        if ($this->addressesData !== null) {
            $count += $this->addressesData->getCount();
        }
        if ($this->containersData !== null) {
            $count += $this->containersData->getCount();
        }
        return $count;
    }

    /**
     * @param PHPExcel $spreadsheetFile
     * @param int $spreadsheetByNumber
     * @return PHPExcel_Worksheet|null
     */
    protected function spreadsheetByNumber(PHPExcel $spreadsheetFile, int $spreadsheetByNumber): ?PHPExcel_Worksheet
    {
        try {
            return $spreadsheetFile->getSheet($spreadsheetByNumber);
        } catch (PHPExcel_Exception $e) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could not get spreadsheet: ' . $e->getMessage())
            );
        }
        return null;
    }

    /**
     * @param PHPExcel_Worksheet $spreadsheetByNumber
     * @param array $headerColumnsMap
     */
    public function buildSpreadsheetHeaderMap(
        PHPExcel_Worksheet $spreadsheetByNumber,
        array &$headerColumnsMap
    ): void {
        try {
            foreach ($spreadsheetByNumber->getRowIterator() as $row) {
                $this->parseHeaderRow($row, $headerColumnsMap);
            }
        } catch (PHPExcel_Exception $e) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could not get spreadsheet data: ' . $e->getMessage())
            );
        }
    }

    /**
     * @param PHPExcel_Worksheet $spreadsheet
     * @param array $headerColumnsMap
     * @param array $allModels
     * @param int $startRow
     * @param Closure|null $autoSaveCallback
     */
    public function spreadsheetDataByHeaderMap(
        PHPExcel_Worksheet $spreadsheet,
        array $headerColumnsMap,
        array &$allModels,
        int $startRow,
        Closure $autoSaveCallback = null
    ): void {
        try {
            foreach ($spreadsheet->getRowIterator($startRow) as $row) {
                Yii::beginProfile('Process row ' . $row->getRowIndex());
                $parsedData = $this->parseDataRow($row, $headerColumnsMap, $spreadsheet);
                if (!empty($parsedData)) {
                    if ($autoSaveCallback instanceof Closure) {
                        $model = call_user_func($autoSaveCallback, $parsedData);
                        unset($model->id);
                        if ($model->save()) {
                            Yii::info(
                                'AgreementAddress saved. ' .
                                'Parsed ID: ' . VarDumper::dumpAsString($parsedData['id']) . ' ' .
                                'Stored ID: ' . VarDumper::dumpAsString($model->id)
                            );
                            array_push($allModels, [
                                'id' => $parsedData['id'],
                                'stored_id' => $model->id,
                            ]);
                        } else {
                            Yii::warning(
                                'Could not save AgreementAddress: ' .
                                VarDumper::dumpAsString($model->errors)
                            );
                        }
                    } else {
                        array_push($allModels, $parsedData);
                    }
                } else {
                    Yii::warning(
                        'Empty data row: ' . $row->getRowIndex()
                    );
                }
                Yii::endProfile('Process row ' . $row->getRowIndex());
            }
        } catch (PHPExcel_Exception $e) {
            Yii::warning(
                Yii::t('app.models.AgreementDataImportForm', 'Could not get spreadsheet data: ' . $e->getMessage())
            );
        }
    }

    /**
     * @param string $spreadsheetFilePath
     * @param bool $autoSave
     * @param int $userSpreadsheetNumber
     * @return array
     */
    public function extractSheetOneDataFromFile(
        string $spreadsheetFilePath,
        bool $autoSave,
        int $userSpreadsheetNumber
    ): array {
        $headerColumnsMap = [
            'CSA Līgums' => ['agreement_number', null, 'formatted'],
            'Līguma datums (CSA Līgums)' => ['agreement_date', null, 'date_njY'],
            'Izvešanu sākuma datums' => ['waste_collection_start_date', null, 'date_njY'],
            'Izvešanu beigu datums' => ['waste_collection_end_date', null, 'date_njY'],
            'Reģ. Nr. (Maksātājs (uzņēmums))' => ['registration_code', null, 'formatted'],
            'Personas Kods (Maksātājs (kontaktpersona))' => ['client_code', null, 'formatted'],
            'E-pasts (Maksātājs (uzņēmums))' => ['company_email', null, 'formatted'],
            'E-pasts (Maksātājs (kontaktpersona))' => ['client_email', null, 'formatted'],
            'Objekts' => ['waste_collection_address', null, 'formatted'],
            'Iela (Objekts)' => ['waste_container_address', null, 'formatted'],
            'Mājas Nr. (Objekts)' => ['building_address', null, 'formatted'],
            'Atkārtošanas nosacījums' => ['waste_collection_frequency', null, 'formatted'],
        ];

        $allModels = [];
        $reader = $this->read_csv($spreadsheetFilePath,50000);

//        $file = fopen($spreadsheetFilePath, 'r');
        while ($reader !== FALSE) {
            //$line is an array of the csv elements
            print_r($reader);
        }
//        fclose($file);

    //        try {
    //            Yii::beginProfile('Extract header');
    //            $reader = PHPExcel_IOFactory::createReader('Excel2007');
    //
    //            /**  Define how many rows we want to read for each "chunk"  **/
////            $chunkSize = 4;
//
//            /**  Create a new Instance of our Read Filter  **/
////            $chunkFilter = new ChunkReadFilter();
//
//            /**  Tell the Reader that we want to use the Read Filter  **/
////            $reader->setReadFilter($chunkFilter);
//
//            /**  Tell the Read Filter that we want only first (header) row **/
////            $chunkFilter->setRows(1, 1);
//
//            /**  Load only the rows that match our filter  **/
////            $spreadsheetFile = $reader->load(
////                $spreadsheetFilePath
////            );
//
//            $spreadsheet = $this->spreadsheetByNumber($spreadsheetFile, $userSpreadsheetNumber);
//
//            if ($spreadsheet !== null) {
//                $this->buildSpreadsheetHeaderMap($spreadsheet, $headerColumnsMap);
//            }
//            Yii::endProfile('Extract header');
//
//            Yii::beginProfile('Extract data');
//
//            /**  Loop to read our worksheet in "chunk size" blocks  **/
//            $lastChunk = false;
//            for ($startRow = 2; $startRow <= 100; $startRow += $chunkSize) {
//                Yii::beginProfile('Process chunk from ' . $startRow . ' to ' . ($startRow + $chunkSize));
//
//                /**  Tell the Read Filter which rows we want this iteration  **/
//                $chunkFilter->setRows($startRow, $chunkSize);
//                /**  Load only the rows that match our filter  **/
//                $spreadsheetFile = $reader->load(
//                    $spreadsheetFilePath
//                );
//
//                $spreadsheet = $this->spreadsheetByNumber($spreadsheetFile, $userSpreadsheetNumber);
//                if ($spreadsheet === null) {
//                    break;
//                }
//
//                // When there is a gap between last row and chunk size, there is no next chunk
//                $highestRowInChunk = $spreadsheet->getHighestRow() + 1;
//                if ($highestRowInChunk < $startRow + $chunkSize) {
//                    $lastChunk = true;
//                }
//
//                $autoSaveCallback = null;
//                if ($autoSave) {
//                    $autoSaveCallback = static function ($parsedData) {
//                        return new AgreementAddress($parsedData);
//                    };
//                }
//                $this->spreadsheetDataByHeaderMap(
//                    $spreadsheet,
//                    $headerColumnsMap,
//                    $allModels,
//                    $startRow,
//                    $autoSaveCallback
//                );
//
//                Yii::endProfile('Process chunk from ' . $startRow . ' to ' . ($startRow + $chunkSize));
//                if ($lastChunk) {
//                    break;
//                }
//            }
//            Yii::endProfile('Extract data');
//        } catch (PHPExcel_Reader_Exception $e) {
//            Yii::$app->session->addFlash(
//                'error',
//                Yii::t('app.models.AgreementDataImportForm', 'Could not load spreadsheet file: ' . $e->getMessage())
//            );
//        }

return $allModels;
}

/**
 * @param string $spreadsheetFilePath
 * @param bool $autoSave
 * @param int $userSpreadsheetNumber
 * @return array
 */
public function extractSheetTwoDataFromFile(
    string $spreadsheetFilePath,
    bool $autoSave,
    int $userSpreadsheetNumber
): array {
    $headerColumnsMap = [
        'Kolonna A' => ['agreement_number', null, false],
        'Kolonna B' => ['agreement_description', null, 'number'],
        'Kolonna C' => ['waste_collection_address', null, 'formatted'],
    ];

    $allModels = [];

    try {
        Yii::beginProfile('Extract header');
        $reader = PHPExcel_IOFactory::createReader('Excel2007');

        /**  Define how many rows we want to read for each "chunk"  **/
        $chunkSize = 4;

        /**  Create a new Instance of our Read Filter  **/
        $chunkFilter = new ChunkReadFilter();

        /**  Tell the Reader that we want to use the Read Filter  **/
        $reader->setReadFilter($chunkFilter);

        /**  Tell the Read Filter that we want only first (header) row **/
        $chunkFilter->setRows(1, 1);

        /**  Load only the rows that match our filter  **/
        $spreadsheetFile = $reader->load(
            $spreadsheetFilePath
        );

        $spreadsheet = $this->spreadsheetByNumber($spreadsheetFile, $userSpreadsheetNumber);

        if ($spreadsheet !== null) {
            $this->buildSpreadsheetHeaderMap($spreadsheet, $headerColumnsMap);
        }
        Yii::endProfile('Extract header');

        Yii::beginProfile('Extract data');

        /**  Loop to read our worksheet in "chunk size" blocks  **/
        $lastChunk = false;
        for ($startRow = 2; $startRow <= 100; $startRow += $chunkSize) {
            Yii::beginProfile('Process chunk from ' . $startRow . ' to ' . ($startRow + $chunkSize));

            /**  Tell the Read Filter which rows we want this iteration  **/
            $chunkFilter->setRows($startRow, $chunkSize);
            /**  Load only the rows that match our filter  **/
            $spreadsheetFile = $reader->load(
                $spreadsheetFilePath
            );

            $spreadsheet = $this->spreadsheetByNumber($spreadsheetFile, $userSpreadsheetNumber);
            if ($spreadsheet === null) {
                break;
            }

            // When there is a gap between last row and chunk size, there is no next chunk
            $highestRowInChunk = $spreadsheet->getHighestRow() + 1;
            if ($highestRowInChunk < $startRow + $chunkSize) {
                $lastChunk = true;
            }

            $autoSaveCallback = null;
            if ($autoSave) {
                $autoSaveCallback = static function ($parsedData) {
                    return new AgreementAddressContainer($parsedData);
                };
            }
            $this->spreadsheetDataByHeaderMap(
                $spreadsheet,
                $headerColumnsMap,
                $allModels,
                $startRow,
                $autoSaveCallback
            );

            Yii::endProfile('Process chunk from ' . $startRow . ' to ' . ($startRow + $chunkSize));
            if ($lastChunk) {
                break;
            }
        }
        Yii::endProfile('Extract data');
    } catch (PHPExcel_Reader_Exception $e) {
        Yii::$app->session->addFlash(
            'error',
            Yii::t('app.models.AgreementDataImportForm', 'Could not load spreadsheet file: ' . $e->getMessage())
        );
    }

    return $allModels;
}

/**
 * This is header row, create Map
 *
 * @param \PHPExcel_Worksheet_Row $row
 * @param array $headerColumnsMap
 * @throws PHPExcel_Exception
 */
protected function parseHeaderRow(\PHPExcel_Worksheet_Row $row, array &$headerColumnsMap): void
{
    $cellIterator = $row->getCellIterator();
    $cellIterator->setIterateOnlyExistingCells(false);
    foreach ($cellIterator as $cell) {
        $headerCellText = $cell->getValue();
        if (array_key_exists(trim($headerCellText), $headerColumnsMap)) {
            $headerColumnsMap[trim($headerCellText)][1] = $cell::columnIndexFromString(
                $cell->getColumn()
            );
        }
    }
}

/**
 * @param \PHPExcel_Worksheet_Row $row
 * @param array $headerColumnsMap
 * @param PHPExcel_Worksheet $spreadsheetNumber
     * @return array|AgreementAddress
     */
    protected function parseDataRow(
        \PHPExcel_Worksheet_Row $row,
        array $headerColumnsMap,
        PHPExcel_Worksheet $spreadsheetNumber
    ) {
        $agreementAddress = [];
        foreach ($headerColumnsMap as $columnInfo) {
            list($attribute, $cellNum, $type) = $columnInfo;
            $cell = $spreadsheetNumber
                ->getCellByColumnAndRow($cellNum - 1, $row->getRowIndex(), false);

            if ($cell === null) {
                $value = null;
            } else {
                if ($type === 'formatted') {
                    $value = $cell->getFormattedValue();
                } elseif ($type === 'date_njY') {
                    $value = $cell->getFormattedValue();
                    if (!empty($value)) {
                        $date = DateTime::createFromFormat(
                            'n/j/Y',
                            $value,
                            new DateTimeZone('Europe/Riga')
                        );
                        if ($date instanceof DateTime) {
                            $date->setTimezone(new DateTimeZone('UTC'));
                            $value = $date->format('Y-m-d 12:00:00');
                        }
                    }
                } elseif ($type === 'number') {
                    $value = $cell->getFormattedValue();
                }else {
                    $value = $cell->getValue();
                }
            }
            if (!empty($value)) {
                $agreementAddress[$attribute] = $value;
            }
        }

        if (!empty($agreementAddress)) {
            $agreementAddress['id'] = ($row->getRowIndex() - 1);
        }
        return $agreementAddress;
    }

    /**
     * @param string $tableName
     * @return bool
     */
    protected function clearTableContents(string $tableName): bool
    {
        $db = Yii::$app->db;
        $transaction = $db->beginTransaction();
        try {
            $db->createCommand('SET foreign_key_checks = 0')->execute();
        } catch (Exception $e) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could not remove foreign key checks: ' . $e->getMessage())
            );
            $transaction->rollBack();
        }

        $query = $db->getQueryBuilder()->truncateTable($tableName);
        try {
            $db->createCommand($query)->execute();
        } catch (Exception $e) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could not truncate table: ' . $e->getMessage())
            );
            $transaction->rollBack();
        }

        try {
            $db->createCommand('SET foreign_key_checks = 1')->execute();
        } catch (Exception $e) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could not set foreign key checks: ' . $e->getMessage())
            );
            $transaction->rollBack();
        }

        try {
            $transaction->commit();
            return true;
        } catch (Exception $e) {
            Yii::error(
                Yii::t('app.models.AgreementDataImportForm', 'Could commit changes: ' . $e->getMessage())
            );
        }
        return false;
    }

    /**
     * @return bool
     */
    protected function clearOne(): bool
    {
        return $this->clearTableContents(AgreementAddress::tableName());
    }

    /**
     * @return bool
     */
    protected function clearTwo(): bool
    {
        return $this->clearTableContents(AgreementAddressContainer::tableName());
    }

    /**
     * @return bool
     */
    protected function clearThree(): bool
    {
        return $this->clearTableContents(AgreementAddressContainer::tableName());
    }

   protected function unique_columns(array $columns):array {
        $values = [];

        foreach ($columns as $value) {
            $count = 0;
            $value = $original = trim($value);

            while (in_array($value, $values)) {
                $value = $original . '-' . ++$count;
            }

            $values[] = $value;
        }

        return $values;
    }


    function read_csv(string $file, int $length = 50000, string $delimiter = '"'): array {
        $handle = fopen($file, 'r');
        $hashes = [];
        $values = [];
        $header = [];
        $headerUnique = null;

        if (!$handle) {
            return $values;
        }

        $header = fgetcsv($handle, $length, $delimiter);

        if (!$header) {
            return $values;
        }

        $headerUnique = $this->unique_columns($header);

        while (false !== ($data = fgetcsv($handle, $length, $delimiter))) {
            $hash = md5(serialize($data));

            if (!isset($hashes[$hash])) {
                $hashes[$hash] = true;
//                $values[] = array_combine($headerUnique, $data);
                $values[] = $headerUnique;
            }
        }

        fclose($handle);

        return $values;
    }


}
