<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%agreement_address__container}}".
 *
 * @property int $id Agreement address container ID
 * @property int|null $agreement_address_id Agreement address ID
 * @property string|null $agreement_number Agreement number
 * @property string|null $agreement_description Agreement description
 * @property string|null $waste_collection_address Waste collection address
 * @property string|null $waste_container_address Waste container address
 * @property string|null $building_address Building address
 * @property string|null $waste_container_type Waste container type
 * @property string|null $waste_container_number Waste container number
 * @property int|null $waste_container_count Waste container count
 * @property string|null $company Company
 * @property string|null $client_name Client name
 *
 * @property-read ActiveQuery $agreementAddressContainers
 * @property AgreementAddress $agreementAddress
 */
class AgreementAddressContainer extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%agreement_address__container}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agreement_address_id', 'waste_container_count'], 'integer'],
            [['agreement_number'], 'string', 'max' => 50],
            [['agreement_description', 'waste_collection_address', 'waste_container_address', 'building_address', 'waste_container_type', 'waste_container_number', 'company', 'client_name'], 'string', 'max' => 255],
            [['agreement_address_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementAddress::class, 'targetAttribute' => ['agreement_address_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.models.AgreementAddressContainer', 'Agreement address container ID'),
            'agreement_address_id' => Yii::t('app.models.AgreementAddressContainer', 'Agreement address ID'),
            'agreement_number' => Yii::t('app.models.AgreementAddressContainer', 'Agreement number'),
            'agreement_description' => Yii::t('app.models.AgreementAddressContainer', 'Agreement description'),
            'waste_collection_address' => Yii::t('app.models.AgreementAddressContainer', 'Waste collection address'),
            'waste_container_address' => Yii::t('app.models.AgreementAddressContainer', 'Waste container address'),
            'building_address' => Yii::t('app.models.AgreementAddressContainer', 'Building address'),
            'waste_container_type' => Yii::t('app.models.AgreementAddressContainer', 'Waste container type'),
            'waste_container_number' => Yii::t('app.models.AgreementAddressContainer', 'Waste container number'),
            'waste_container_count' => Yii::t('app.models.AgreementAddressContainer', 'Waste container count'),
            'company' => Yii::t('app.models.AgreementAddressContainer', 'Company'),
            'client_name' => Yii::t('app.models.AgreementAddressContainer', 'Client name'),
        ];
    }

    /**
     * Gets query for [[AgreementAddressContainers]].
     *
     * @return ActiveQuery
     */
    public function getAgreementAddressContainers()
    {
        return $this->hasMany(AgreementAddressContainer::class, ['agreement_address_id' => 'id']);
    }

}
