<?php

namespace app\models;

use Yii;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%agreement_address}}".
 *
 * @property int $id Agreement address ID
 * @property string|null $agreement_number Agreement number
 * @property string|null $agreement_date Agreement date
 * @property string|null $waste_collection_start_date Waste collection start date
 * @property string|null $waste_collection_end_date Waste collection end date or agreement end date
 * @property string|null $registration_code Registration code
 * @property string|null $client_code Client code or registration code
 * @property string|null $company_email Company e-mail address
 * @property string|null $client_email Client e-mail address
 * @property string|null $waste_collection_address Waste collection address
 * @property string|null $waste_container_address Waste container address
 * @property string|null $building_address Building address
 * @property string|null $waste_collection_frequency Waste collection frequency
 *
 * @property AgreementAddressContainer[] $agreementAddressContainers
 */
class AgreementAddress extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%agreement_address}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['agreement_date', 'waste_collection_start_date', 'waste_collection_end_date'], 'safe'],
            [['agreement_number'], 'string', 'max' => 50],
            [['registration_code', 'client_code', 'company_email', 'client_email', 'waste_collection_address', 'waste_container_address', 'building_address', 'waste_collection_frequency'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app.models.AgreementAddress', 'Agreement address ID'),
            'agreement_number' => Yii::t('app.models.AgreementAddress', 'Agreement number'),
            'agreement_date' => Yii::t('app.models.AgreementAddress', 'Agreement date'),
            'waste_collection_start_date' => Yii::t('app.models.AgreementAddress', 'Waste collection start date'),
            'waste_collection_end_date' => Yii::t('app.models.AgreementAddress', 'Waste collection end date or agreement end date'),
            'registration_code' => Yii::t('app.models.AgreementAddress', 'Registration code'),
            'client_code' => Yii::t('app.models.AgreementAddress', 'Client code'),
            'company_email' => Yii::t('app.models.AgreementAddress', 'Company e-mail address'),
            'client_email' => Yii::t('app.models.AgreementAddress', 'Client e-mail address'),
            'waste_collection_address' => Yii::t('app.models.AgreementAddress', 'Waste collection address'),
            'waste_container_address' => Yii::t('app.models.AgreementAddress', 'Waste container address'),
            'building_address' => Yii::t('app.models.AgreementAddress', 'Building address'),
            'waste_collection_frequency' => Yii::t('app.models.AgreementAddress', 'Waste collection frequency'),
        ];
    }

    /**
     * Gets query for [[AgreementAddress]].
     *
     * @return ActiveQuery
     */
    public function getAgreementAddress()
    {
        return $this->hasOne(AgreementAddress::class, ['id' => 'agreement_address_id']);
    }

}
