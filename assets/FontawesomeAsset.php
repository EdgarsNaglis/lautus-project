<?php

namespace app\assets;

use yii\web\AssetBundle;

/**
 * Assets for bower package: fontawesome
 *
 * @author    Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2015 SIA "Web Multishop Company"
 */
class FontawesomeAsset extends AssetBundle
{
    public $sourcePath = '@bower/fontawesome';
    public $baseUrl = '@web';
    public $css = [
        'css/font-awesome.css',
    ];
}
