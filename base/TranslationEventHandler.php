<?php
namespace app\base;

use Yii;
use yii\i18n\MissingTranslationEvent;

/**
 * Class TranslationEventHandler
 *
 * @package app\base
 */
class TranslationEventHandler
{

    /**
     * @param MissingTranslationEvent $event
     */
    public static function handleMissingTranslation(MissingTranslationEvent $event)
    {
        Yii::warning("Translation missing: {$event->category}.{$event->message} for language {$event->language}");
    }
}