<?php
namespace app\base;

use Yii;
use yii\helpers\Url;
use yii\web\View;

/**
 * Class Controller
 *
 * @author Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2015 SIA "Web Multishop Company"
 *
 * @package app\base
 */
class Controller extends \yii\web\Controller {

    public $heading;
    public $subHeading;
    public $metaData = [];

    public function registerGoogleAnalytics()
    {
        if (!YII_DEBUG && isset(Yii::$app->params['googleAnalytics'])) {
            $ga = Yii::$app->params['googleAnalytics'];
            if (isset($ga['account']) && strlen($ga['account']) > 0) {
                $account = $ga['account'];
                $this->view->registerJs(<<<JS
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', '$account', 'auto');
ga('send', 'pageview');
JS
                    , View::POS_END
                );
            }
        }
    }

    public function registerIcons()
    {
        if (isset(Yii::$app->params['faviconData'])) {
            $faviconData = Yii::$app->params['faviconData'];
            $iconSizes = [
                'apple-touch-icon' => [
                    '57x57',
                    '60x60',
                    '72x72',
                    '76x76',
                    '114x114',
                    '120x120',
                    '144x144',
                    '152x152',
                    '180x180',
                ],
                'icon' => [
                    '16x16',
                    '32x32',
                    '96x96',
                    '192x192'
                ]
            ];
            foreach ($iconSizes as $iconSizeGroupName => $iconSizeGroup) {
                foreach ($iconSizeGroup as $iconSize) {
                    if (isset($faviconData[$iconSizeGroupName.'_'.$iconSize])) {
                        $this->view->registerLinkTag([
                            'rel'   => $iconSizeGroupName,
                            'sizes' => $iconSize,
                            'href'  => Url::base() . $faviconData[$iconSizeGroupName.'_'.$iconSize],
                            'type'  => 'image/png'
                        ]);
                    }
                }
            }
            if (isset($faviconData['manifest'])) {
                $this->view->registerLinkTag([
                    'rel' => 'manifest',
                    'href' => Url::base() . $faviconData['manifest']
                ]);
            }
            if (isset($faviconData['shortcut-icon'])) {
                $this->view->registerLinkTag([
                    'rel' => 'shortcut icon',
                    'href' => Url::base() . $faviconData['shortcut-icon']
                ]);
            }
            if (isset($faviconData['msapplication-TileColor'])) {
                $this->view->registerLinkTag([
                    'name' => 'msapplication-TileColor',
                    'content' => $faviconData['msapplication-TileColor']
                ]);
            }
            if (isset($faviconData['msapplication-TileImage'])) {
                $this->view->registerLinkTag([
                    'name' => 'msapplication-TileImage',
                    'content' => Url::base() . $faviconData['msapplication-TileImage']
                ]);
            }
            if (isset($faviconData['msapplication-config'])) {
                $this->view->registerLinkTag([
                    'name' => 'msapplication-config',
                    'content' => Url::base() . $faviconData['msapplication-config']
                ]);
            }
            if (isset($faviconData['theme-color'])) {
                $this->view->registerLinkTag([
                    'name' => 'theme-color',
                    'content' => $faviconData['theme-color']
                ]);
            }
        }
    }

    public function registerMeta()
    {
        if (!isset($this->metaData['locale'])) {
            $this->metaData['locale'] = Yii::$app->params['site']['locale'];
        }
        $this->view->registerMetaTag([
            'property' => 'og:locale',
            'content' => $this->metaData['locale']
        ]);

        if (!isset($this->metaData['site_name'])) {
            $this->metaData['site_name'] = Yii::$app->params['site']['name'];
        }
        $this->view->registerMetaTag([
            'property' => 'og:site_name',
            'content' => $this->metaData['site_name']
        ]);

        if (!isset($this->metaData['url'])) {
            $this->metaData['url'] = Yii::$app->request->url;
        }
        $this->view->registerMetaTag([
            'property' => 'og:url',
            'content' => $this->metaData['url']
        ]);

        if (!isset($this->metaData['image'])) {
            $this->metaData['image'] = Yii::$app->params['site']['logo'];
        }
        if (strlen($this->metaData['image']) > 0) {
            $this->view->registerMetaTag([
                'property' => 'og:image',
                'content'  => Url::base() . $this->metaData['image']
            ]);
        }

        if (isset($this->metaData['type'])) {
            $this->view->registerMetaTag([
                'property' => 'og:type',
                'content' => $this->metaData['type']
            ]);
        }
        if (isset($this->metaData['title'])) {
            $this->view->registerMetaTag([
                'property' => 'og:title',
                'content' => $this->metaData['title']
            ]);
        }
        if (isset($this->metaData['description']) && strlen($this->metaData['description']) > 0) {
            $this->view->registerMetaTag([
                'property' => 'og:description',
                'content' => $this->metaData['description']
            ]);
            $this->view->registerMetaTag([
                'name' => 'description',
                'content' => $this->metaData['description']
            ]);
        }
    }
}