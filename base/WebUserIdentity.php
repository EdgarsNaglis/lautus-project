<?php
namespace app\base;

use app\models\User;
use Yii;
use yii\base\NotSupportedException;
use yii\base\BaseObject;
use yii\web\IdentityInterface;

/**
 * Class WebUserIdentity
 *
 * @author Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2016 SIA "Web Multishop Company"
 *
 * @package app\base
 *
 * @property-read void $authKey
 * @property-read null $id
 * @property-read null|string $username
 */
class WebUserIdentity extends BaseObject implements IdentityInterface
{

    /**
     * @var User
     */
    public $user;

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        if ($user = User::findOne(['id' => $id])) {
            $identity = new WebUserIdentity();
            $identity->user = $user;
            return $identity;
        }
        return null;
    }

    /**
     * @inheritdoc
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername(string $username)
    {
        if ($user = User::findOne(['username' => $username])) {
            $identity = new WebUserIdentity();
            $identity->user = $user;
            return $identity;
        }
        return null;
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return ($this->user ? $this->user->getPrimaryKey() : null);
    }

    /**
     * @inheritdoc
     * @throws NotSupportedException
     */
    public function getAuthKey()
    {
        throw new NotSupportedException;
    }

    /**
     * @inheritdoc
     * @throws NotSupportedException
     */
    public function validateAuthKey($authKey)
    {
        throw new NotSupportedException;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword(string $password)
    {
        return ($this->user ? $this->user->validatePassword($password) : null);
    }

    /**
     * Returns current user username
     *
     * @return null|string
     */
    public function getUsername()
    {
        return ($this->user ? $this->user->username : null);
    }

    /**
     * @inheritdoc
     */
    public function getRateLimit($request, $action)
    {
        return [100, 600];
    }

    /**
     * @inheritdoc
     */
    public function loadAllowance($request, $action)
    {
        $cache = \Yii::$app->cache;
        return [
            $cache->get($request->pathInfo . $request->method . '_remaining'),
            $cache->get($request->pathInfo . $request->method . '_ts')
        ];
    }

    /**
     * @inheritdoc
     */
    public function saveAllowance($request, $action, $allowance, $timestamp)
    {
        $cache = \Yii::$app->cache;
        $cache->set($request->pathInfo . $request->method . '_remaining', $allowance);
        $cache->set($request->pathInfo . $request->method . '_ts', $timestamp);
    }
}
