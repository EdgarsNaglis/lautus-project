<?php
namespace app\base;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseHtml;
use yii\helpers\Url;

/**
 * Extended Html provides a set of static methods for generating commonly used HTML tags.
 *
 * @author    Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2015 Web Multishop Company LLC
 *
 * @inheritdoc
 */
class ExtendedHtml extends BaseHtml
{

    /**
     * Generates a script tag that refers to an external Template file.
     * @param string $url the URL of the external Template file. This parameter will be processed by [[Url::to()]].
     * @param array $options the tag options in terms of name-value pairs. The options will be rendered as the
     * attributes of the resulting script tag. The values will be HTML-encoded using [[encode()]]. If a value is null,
     * the corresponding attribute will not be rendered.
     * See [[renderTagAttributes()]] for details on how attributes are being rendered.
     * @return string the generated script tag
     * @see Url::to()
     */
    public static function templateFile($url, $options = [])
    {
        $url = Url::to($url);
        if (!isset($options['type'])) {
            $options['type'] = 'text/template';
        }
        $retrieve = ArrayHelper::remove($options, 'retrieve', false);
        if ($retrieve) {
            $template = '';
            if (Url::isRelative($url)) {
                $path = Yii::getAlias($url);
                if (file_exists($path)) {
                    $template = file_get_contents($path);
                }
            }
            else {
                $curl = curl_init();
                curl_setopt($curl, CURLOPT_URL, $url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HEADER, false);
                $template = curl_exec($curl);
                curl_close($curl);
            }
            return static::tag('script', $template, $options);
        }
        else {
            $options['src'] = $url;
            return static::tag('script', '', $options);
        }
    }
}
