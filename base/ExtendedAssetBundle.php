<?php
namespace app\base;

use Yii;
use yii\helpers\Url;
use yii\web\AssetBundle;

/**
 * Extended asset bundle that introduces template files as assets
 *
 * @author    Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2015 Web Multishop Company LLC
 */
class ExtendedAssetBundle extends AssetBundle
{

    /**
     * @var array list of Template files that this bundle contains. Each Template file can be
     * specified in format:
     *
     * - a relative path representing a local asset (e.g. `templates/main.ejs`). The actual file path of a local
     *   asset can be determined by prefixing [[basePath]] to the relative path, and the actual URL
     *   of the asset can be determined by prefixing [[baseUrl]] to the relative path.
     *
     * Note that only forward slash "/" should be used as directory separators.
     */
    public $template = [];

    /**
     * @var array the options that will be passed to [[View::registerTemplateFile()]]
     * when registering the Template files in this bundle.
     */
    public $templateOptions = [];

    /**
     * Registers the CSS, JS and Template files with the given view.
     *
     * @param ExtendedView $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);
        $manager = $view->getAssetManager();
        foreach ($this->template as $template) {
            if (isset($this->templateOptions['retrieve']) && $this->templateOptions['retrieve'] && Url::isRelative($template)) {
                $url = $manager->getAssetPath($this, $template);
            }
            else {
                $url = $manager->getAssetUrl($this, $template);
            }
            $view->registerTemplateFile($url, $this->templateOptions);
        }
    }

    /**
     * @inheritdoc
     */
    public function publish($am)
    {
        parent::publish($am);

        if (isset($this->basePath, $this->baseUrl) && ($converter = $am->getConverter()) !== null) {
            foreach ($this->template as $i => $template) {
                if (Url::isRelative($template)) {
                    $this->template[$i] = $converter->convert($template, $this->basePath);
                }
            }
        }
    }
}