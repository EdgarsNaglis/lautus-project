<?php
namespace app\base;

use Yii;
use yii\base\InvalidParamException;
use yii\helpers\ArrayHelper;
//use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * Extended view that introduces template file registration
 *
 * @author    Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2015 Web Multishop Company LLC
 */
class ExtendedView extends View
{

    /**
     * @var array the registered Template code blocks
     * @see registerJs()
     */
    public $template;

    /**
     * @var array the registered Template files.
     * @see registerJsFile()
     */
    public $templateFiles;

    /**
     * @var integer Where the templates registered using {@link registerTemplate} will be inserted in the page.
     * This can be one of the CClientScript::POS_* constants, except "View::POS_LOAD" and "View::POS_READY".
     * Defaults to View::POS_HEAD.
     */
    public $defaultTemplatePosition = self::POS_HEAD;

    /**
     * Clears up the registered meta tags, link tags, css/js scripts and files.
     */
    public function clear()
    {
        parent::clear();
        $this->template = null;
        $this->templateFiles = null;
    }

    /**
     * Registers a Template file.
     * @param string $url the Template file to be registered.
     * @param array $options the HTML attributes for the script tag. The following options are specially handled
     * and are not treated as HTML attributes:
     *
     * - `retrieve`: bool, specifies whether Template file should be retrieved and included as code block
     * - `depends`: array, specifies the names of the asset bundles that this Template file depends on.
     * - `position`: specifies where the Template script tag should be inserted in a page. The possible values are:
     *     * [[POS_HEAD]]: in the head section
     *     * [[POS_BEGIN]]: at the beginning of the body section
     *     * [[POS_END]]: at the end of the body section. This is the default value.
     *
     * Please refer to [[ExtendedHtml::templateFile()]] for other supported options.
     *
     * @param string $key the key that identifies the Template script file. If null, it will use
     * filename from $url as the key. If two Template files are registered with the same key, the latter
     * will overwrite the former.
     */
    public function registerTemplateFile($url, $options = [], $key = null) {
        $url = Yii::getAlias($url);
        $key = $key ?: pathinfo($url)['filename'];
        $depends = ArrayHelper::remove($options, 'depends', []);

        if (empty($depends)) {
            $position = ArrayHelper::remove($options, 'position', self::POS_HEAD);
            if ($position == self::POS_LOAD || $position == self::POS_READY) {
                throw new InvalidParamException(
                    'Templates are not allowed in &quot;View::POS_LOAD&quot; and &quot;View::POS_READY&quot;.'
                );
            }
            if (!isset($options['id'])) {
                $options['id'] = $key;
            }
            $this->templateFiles[$position][$key] = ExtendedHtml::templateFile($url, $options);
        } else {
            $this->getAssetManager()->bundles[$key] = new ExtendedAssetBundle([
                'baseUrl' => '',
                'template' => [strncmp($url, '//', 2) === 0 ? $url : ltrim($url, '/')],
                'templateOptions' => $options,
                'depends' => (array) $depends,
            ]);
            $this->registerAssetBundle($key);
        }
    }

    /**
     * Registers a Template code block.
     * @param string $template the Template code block to be registered
     * @param integer $position the position at which the Template script tag should be inserted
     * in a page. The possible values are:
     *
     * - [[POS_HEAD]]: in the head section
     * - [[POS_BEGIN]]: at the beginning of the body section
     * - [[POS_END]]: at the end of the body section
     *
     * @param string $key the key that identifies the Template code block. If null, it will use
     * $template as the key. If two Template code blocks are registered with the same key, the latter
     * will overwrite the former.
     */
    public function registerTemplate($template, $position = self::POS_HEAD, $key = null)
    {
        $key = $key ?: md5($template);
        $this->template[$position][$key] = $template;
    }

    /**
     * Renders the content to be inserted in the head section.
     * The content is rendered using the registered meta tags, link tags, CSS/JS code blocks and files.
     * @return string the rendered content
     */
    protected function renderHeadHtml()
    {
        $lines = [];
        if (!empty($this->metaTags)) {
            $lines[] = implode("\n", $this->metaTags);
        }

        if (!empty($this->linkTags)) {
            $lines[] = implode("\n", $this->linkTags);
        }
        if (!empty($this->cssFiles)) {
            $lines[] = implode("\n", $this->cssFiles);
        }
        if (!empty($this->css)) {
            $lines[] = implode("\n", $this->css);
        }
        if (!empty($this->jsFiles[self::POS_HEAD])) {
            $lines[] = implode("\n", $this->jsFiles[self::POS_HEAD]);
        }
        if (!empty($this->js[self::POS_HEAD])) {
            $lines[] = Html::script(implode("\n", $this->js[self::POS_HEAD]), ['type' => 'text/javascript']);
        }
        if (!empty($this->jsFiles[self::POS_HEAD])) {
            $lines[] = implode("\n", $this->jsFiles[self::POS_HEAD]);
        }
        if (!empty($this->template[self::POS_HEAD])) {
            foreach ($this->template[self::POS_HEAD] as $key => $template) {
                $lines[] = Html::script($template, ['type' => 'text/template', 'id' => $key]);
            }
        }
        if (!empty($this->templateFiles[self::POS_HEAD])) {
            $lines[] = implode("\n", $this->templateFiles[self::POS_HEAD]);
        }

        return empty($lines) ? '' : implode("\n", $lines);
    }
}