<?php
namespace app\base;

use Yii;
use yii\db\Migration;

/**
 * Class ExtendedMigration extends default Migration class with more helper methods
 *
 * @package app\base
 *
 * @author    Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2015 SIA "Web Multishop Company"
 */
class ExtendedMigration extends Migration {


    /**
     * Creates MySQL syntax for table column comment
     *
     * @param string $comment
     *
     * @return string
     */
    public function comment($comment)
    {
        return " COMMENT '$comment'";
    }

    /**
     * Creates table name surrounded by {{% and }} used for table name prefixing
     *
     * @param string $table
     *
     * @return string
     */
    public function prefixedTable($table)
    {
        return '{{%' . $table . '}}';
    }

    const FK_RESTRICT = 1;
    const FK_CASCADE = 2;
    const FK_SET_NULL = 3;
    const FK_NO_ACTION = 4;

    /**
     * Creates forgeign key creation string
     *
     * @param string $table
     * @param string $column
     * @param string $refTable
     * @param string $refColumns
     * @param int|string $onDelete
     * @param int|string $onUpdate
     *
     * @return string
     */
    public function foreginKey($table, $column, $refTable, $refColumns, $onDelete = 1, $onUpdate = 1)
    {
        $db = Yii::$app->db;
        $options = [
            self::FK_RESTRICT => 'RESTRICT',
            self::FK_CASCADE => 'CASCADE',
            self::FK_SET_NULL => 'SET NULL',
            self::FK_NO_ACTION => 'NO ACTION',
        ];
        return 'CONSTRAINT `fk_'.$table.'_'.$column.'` '.
        'FOREIGN KEY ('.$db->quoteColumnName($column).') '.
        'REFERENCES ' . $this->prefixedTable($refTable) . ' ('.$db->quoteColumnName($refColumns).') '.
        'ON DELETE '.(is_numeric($onDelete) ? $options[$onDelete] : $options).' '.
        'ON UPDATE '.(is_numeric($onUpdate) ? $options[$onUpdate] : $options);
    }

    /**
     * @inheritdoc
     */
    public function addForeignKey($name = null, $table, $columns, $refTable, $refColumns, $delete = null, $update = null)
    {
        $options = [
            self::FK_RESTRICT => 'RESTRICT',
            self::FK_CASCADE => 'CASCADE',
            self::FK_SET_NULL => 'SET NULL',
            self::FK_NO_ACTION => 'NO ACTION',
        ];
        if (is_null($name)) {
            $name = 'fk_'.$table.'_'.$columns;
        }
        parent::addForeignKey(
            $name,
            $this->prefixedTable($table),
            $columns,
            $refTable,
            $refColumns,
            (is_numeric($delete) ? $options[$delete] : $options),
            (is_numeric($update) ? $options[$update] : $options)
        );
    }

    /**
     * Builds and executes a SQL statement for creating a new index.
     *
     * @param string $table the table that the new index will be created for. The table name will be properly quoted by the method.
     * @param string|array $columns the column(s) that should be included in the index. If there are multiple columns, please separate them
     * by commas or use an array. Each column name will be properly quoted by the method. Quoting will be skipped for column names that
     * include a left parenthesis "(".
     * @param boolean $unique whether to add UNIQUE constraint on the created index.
     * @param string $name the name of the index. The name will be properly quoted by the method.
     */
    public function createIndex($table, $columns, $unique = false, $name = null)
    {
        if (is_null($name)) {
            if (is_array($columns)) {
                $columnsStr = implode('_', $columns);
            }
            else {
                $columnsStr = str_replace(',', '_', $columns);
            }
            $name = 'idx_'.$table.'_'.$columnsStr;
        }
        parent::createIndex($name, $this->prefixedTable($table), $columns, $unique);
    }

    /**
     * @param array $tablesArray
     */
    public function dropTables($tablesArray) {
        foreach ($tablesArray as $tableName) {
            $this->dropTable($this->prefixedTable($tableName));
        }
    }

    /**
     * Outputs an error message
     *
     * @param string $message
     */
    public function outputError($message)
    {
        echo "Error: $message\n";
    }

    /**
     * Returns true, if given tables does exist
     *
     * @param string|array $tableNames
     * @param bool  $outputErrors
     *
     * @return bool
     */
    public function tablesExist($tableNames, $outputErrors = true)
    {
        if (is_string($tableNames)) {
            $tableNames = explode(',', $tableNames);
        }
        $tablesExist = true;
        foreach ($tableNames as $tableName) {
            if (Yii::$app->db->schema->getTableSchema($this->prefixedTable($tableName)) === null) {
                if ($outputErrors) {
                    $this->outputError('"'.$tableName.'" table does not exist');
                }
                $tablesExist = false;
            }
        }
        return $tablesExist;
    }
}