<?php

namespace app\base\web;

use yii\web\Application as YiiApplication;

/**
 * Base web Application class is the base class for all web application controllers.
 *
 * @property-read Application $app
 *
 * @author Nils (Deele) <deele@tuta.io>
 *
 * @package app\base\web
 */
class Application extends YiiApplication
{
}
