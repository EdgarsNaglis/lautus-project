<?php
namespace app\base;

use DateTime;
use DateTimeZone;
use Exception;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class DateFormatter
 *
 * @author Nils Lindentals <nils@webmultishop.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\base
 */
class DateFormatter
{

    /**
     * @param      $date
     * @param null $format
     *
     * @return DateTime|string
     */
    public static function formatUtcDate($date, $format = null)
    {
        if (!static::isEmptyDateTime($date)) {
            try {
                $date = new DateTime($date, new DateTimeZone('UTC'));
            } catch (Exception $e) {
                Yii::error(
                    'Could not format date: ' . $e->getMessage()
                );
                return '';
            }
            if ($format !== null) {
                return $date->format($format);
            }

            return $date;
        }

        return '';
    }

    /**
     * @return array
     */
    public static function weekdayNames()
    {
        return [
            Yii::t('app', 'Monday'),
            Yii::t('app', 'Tuesday'),
            Yii::t('app', 'Thursday'),
            Yii::t('app', 'Wednesday'),
            Yii::t('app', 'Friday'),
            Yii::t('app', 'Saturday'),
            Yii::t('app', 'Sunday'),
        ];
    }

    /**
     * @return array
     */
    public static function monthNames()
    {
        return [
            1 => Yii::t('app', 'January'),
            2 => Yii::t('app', 'February'),
            3 => Yii::t('app', 'March'),
            4 => Yii::t('app', 'April'),
            5 => Yii::t('app', 'May'),
            6 => Yii::t('app', 'June'),
            7 => Yii::t('app', 'July'),
            8 => Yii::t('app', 'August'),
            9 => Yii::t('app', 'September'),
            10 => Yii::t('app', 'October'),
            11 => Yii::t('app', 'November'),
            12 => Yii::t('app', 'December'),
        ];
    }

    /**
     * @param DateTime|string $date
     *
     * @return string
     */
    public static function formatDate($date)
    {
        if (!($date instanceof DateTime)) {
            try {
                $date = new DateTime($date);
            } catch (Exception $e) {
                Yii::error(
                    'Could not format date: ' . $e->getMessage()
                );
                return '';
            }
        }
        $weekdayNames = self::weekdayNames();
        $monthNames = self::monthNames();
        return Yii::t(
            'app',
            '{year}. gada, {day}. {monthName}, {hours}:{minutes}',
            [
                'year' => $date->format('Y'),
                'month' => $date->format('n'),
                'monthName' => $monthNames[$date->format('n')],
                'day' => $date->format('j'),
                'weekday' => $date->format('N'),
                'weekdayName' => $weekdayNames[$date->format('N') - 1],
                'hours' => $date->format('H'),
                'minutes' => $date->format('i'),
            ]
        );
    }

    /**
     * @param string $dateTime
     * @return bool
     */
    public static function isEmptyDateTime($dateTime)
    {
        return !is_string($dateTime) || empty($dateTime) || $dateTime === '0000-00-00 00:00:00';
    }

    /**
     * @param string $dateTime
     * @param array $options
     * @return mixed|string|null
     */
    public static function convertDateTimeTimezone($dateTime, $options = [])
    {
        try {
            if (static::isEmptyDateTime($dateTime)) {
                return ArrayHelper::getValue($options, 'defaultValue', null);
            }
            $timeZoneFrom = ArrayHelper::getValue($options, 'timeZoneFrom', 'UTC');
            $timeZoneTo = ArrayHelper::getValue($options, 'timeZoneTo', Yii::$app->timeZone);
            $format = ArrayHelper::getValue($options, 'format', 'Y-m-d H:i:s');
        } catch (Exception $e) {
            Yii::error(
                'Could not begin date and time conversion: ' . $e->getMessage()
            );
            return '';
        }
        try {
            $dateTimeObject = (new DateTime(
                $dateTime,
                new DateTimeZone($timeZoneFrom)
            ))
                ->setTimezone(new DateTimeZone($timeZoneTo));
        } catch (Exception $e) {
            Yii::error(
                'Could not format date: ' . $e->getMessage()
            );
            return '';
        }
        if ($format === null) {
            return $dateTimeObject;
        }

        return $dateTimeObject->format($format);
    }

    /**
     * @param object $model
     * @param string $attribute
     * @param array $options
     */
    public static function convertModelDateTimeTimezoneForDisplay(&$model, $attribute, $options = [])
    {
        $model->{$attribute} = static::convertDateTimeTimezone(
            $model->{$attribute},
            ArrayHelper::merge(
                [
                    'timeZoneFrom' => 'UTC',
                    'timeZoneTo' => Yii::$app->timeZone,
                ],
                $options
            )
        );
    }

    /**
     * @param object $model
     * @param string $attribute
     * @param array $options
     */
    public static function convertModelDateTimeTimezoneForStorage(&$model, $attribute, $options = [])
    {

        if (empty($model->{$attribute})) {
            $model->{$attribute} = null;
        } else {
            $model->{$attribute} = static::convertDateTimeTimezone(
                $model->{$attribute},
                ArrayHelper::merge(
                    [
                        'timeZoneFrom' => Yii::$app->timeZone,
                        'timeZoneTo' => 'UTC',
                    ],
                    $options
                )
            );
        }
    }
}