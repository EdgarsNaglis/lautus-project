<?php
/**
 * Contains \base\ExportTable
 *
 * @link http://www.webmultishop.com/
 * @copyright 2016 SIA "Web Multishop Company"
 * @license http://www.webmultishop.com/license/
 */

namespace app\base;

use Closure;
use PHPExcel;
use PHPExcel_Cell;
use PHPExcel_IOFactory;
use Yii;
use yii\base\Object;
use yii\data\BaseDataProvider;
use yii\helpers\ArrayHelper;
use yii\helpers\Inflector;
use yii\helpers\VarDumper;

/**
 * ExportTable class provides table exporting
 *
 * @author Nils Lindentals <nils@webmultishop.com>
 */
class ExportTable extends Object
{
    const FORMAT_XLS = 'xls';

    /**
     * @var string
     */
    public $format;

    /**
     * @var array
     */
    public $options;

    /**
     * @var PHPExcel
     */
    protected $objPHPExcel;

    /**
     * @var null
     */
    protected $activePage = null;

    /**
     * @var int
     */
    protected $totalPages = 0;

    /**
     * @var array
     */
    protected $pageActiveRow = [];

    /**
     * @return bool|null|\PHPExcel_Worksheet
     */
    public function getActivePage()
    {
        if ($this->format == self::FORMAT_XLS) {
            try {
                return $this
                    ->objPHPExcel
                    ->getActiveSheet();
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }
        return null;
    }

    /**
     * @param $title
     * @param null $page
     *
     * @return bool|null
     */
    public function titlePage($title, $page = null)
    {
        if ($this->format == self::FORMAT_XLS) {
            try {
                $title = substr(
                    str_replace(
                        ['*',':','/','\\','?','[',']'],
                        '',
                        $title
                    ),
                    0,
                    30
                );
                if (is_null($page)) {
                    $page = $this->getActivePage();
                } else {
                    $page = $this
                        ->objPHPExcel
                        ->getSheet($page);
                }
                $page->setTitle($title);
                return true;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }
        return null;
    }

    /**
     * @param int $page
     *
     * @return bool|null
     */
    public function switchPage($page)
    {
        if ($this->format == self::FORMAT_XLS) {
            try {
                Yii::warning('WWWWWW . ' . $page);


                $this->objPHPExcel->setActiveSheetIndex($page - 1);
                $this->activePage = $page;
                return true;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }

        return null;
    }

    /**
     * @param bool|true $switchToNewPage
     * @param null $title
     *
     * @return bool|int|null
     */
    public function createNewPage(
        $switchToNewPage = true,
        $title = null
    ) {
        if ($this->format == self::FORMAT_XLS) {
            try {
                Yii::warning('XXXX page ' . $this->totalPages);
                $this->totalPages++;
                Yii::warning('XXXX page incr ' . $this->totalPages);
                Yii::warning('$switchToNewPage page incr ' . VarDumper::dumpAsString($switchToNewPage));
                $newPage = $this->totalPages;
                Yii::warning('$newPage  ' . VarDumper::dumpAsString($newPage));
                $this->pageActiveRow[$newPage] = 1;
                $this->objPHPExcel->createSheet($newPage);
                if (!is_null($title)) {
                    $this->titlePage($title, $newPage - 1);
                }
                if ($switchToNewPage) {
                    $this->switchPage($newPage);
                }
                return $newPage;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }

        return null;
    }

    /**
     * @param null|int $page
     * @return mixed
     */
    public function getPageActiveRow($page = null)
    {
        if (is_null($page)) {
            $page = $this->activePage;
        }
        return $this->pageActiveRow[$page];
    }

    /**
     * @param null|int $page
     */
    public function incrementPageActiveRow($page = null)
    {
        if (is_null($page)) {
            $page = $this->activePage;
        }
        $this->pageActiveRow[$page]++;
    }

    /**
     * @return bool|int|null
     */
    public function deleteAllPages()
    {
        if ($this->format == self::FORMAT_XLS) {
            try {
                $this->objPHPExcel->disconnectWorksheets();
                $this->totalPages = 0;
                $this->activePage = null;
                $this->pageActiveRow = [];
                return $this->totalPages;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }

        return null;
    }

    /**
     * @param $title
     *
     * @return bool|null
     */
    public function setTitle($title)
    {
        if ($this->format == self::FORMAT_XLS) {
            try {
                $this
                    ->objPHPExcel
                    ->getProperties()
                    ->setTitle($title);
                return true;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }
        return null;
    }

    /**
     * @param string $format
     * @param array $options
     *
     * @return bool|null
     */
    public function prepareFormat($format, $options = null)
    {
        if ($format == self::FORMAT_XLS) {
            $this->format = self::FORMAT_XLS;
            if (!is_null($options)) {
                $this->options = $options;
            }
            try {
                $this->objPHPExcel = new PHPExcel();
                $title = ArrayHelper::getValue(
                    $this->options,
                    'title',
                    'Untitled'
                );
                $this
                    ->objPHPExcel
                    ->getProperties()
                    ->setTitle($title);

                $this->deleteAllPages();
                $this->createNewPage();
                return true;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }
        return null;
    }

    /**
     * @param BaseDataProvider $dataProvider
     * @param $options
     *
     * @return bool|null
     *
     */
    public function loadDataProviderData(
        $dataProvider,
        $options
    ) {
        $indexOptions = ArrayHelper::getValue($options, 'indexOptions', []);
        $columns = ArrayHelper::getValue($options, 'columns', []);
        $headerOptions = ArrayHelper::getValue($options, 'headerOptions', []);
        $stripTags = ArrayHelper::getValue($options, 'stripTags', true);

//        ini_set('memory_limit', '1024M');
        ini_set('memory_limit', '2048M');
        ini_set('max_execution_time', 600);
        $cacheMethod = \PHPExcel_CachedObjectStorageFactory::cache_to_sqlite3;
        \PHPExcel_Settings::setCacheStorageMethod($cacheMethod);



        if ($this->format == self::FORMAT_XLS) {
            $dataProvider->pagination = false;
            $models = $dataProvider->getModels();
            /// @todo Needs a batch processing / pagination algorithm as this uses way too much memory
//            Yii::trace('$dataProvider->pagination => '. VarDumper::dumpAsString($dataProvider->pagination));
//            Yii::trace('$models sizeof => ' .  VarDumper::dumpAsString(sizeof($models)));
            $page = $this->getActivePage();
            try {

                // Index row
                if (ArrayHelper::getValue($indexOptions, 'showIndex', false)) {
                    for ($activeCol = 0; $activeCol < count($columns); $activeCol++) {
                        $page
                            ->setCellValue(
                                PHPExcel_Cell::stringFromColumnIndex(
                                    $activeCol
                                ) . $this->getPageActiveRow(),
                                $activeCol + 1
                            );
                    }
                    if (ArrayHelper::getValue($indexOptions, 'bold', true)) {
                        $page->getStyle(
                            PHPExcel_Cell::stringFromColumnIndex(0) .
                            $this->getPageActiveRow() .
                            ':' .
                            PHPExcel_Cell::stringFromColumnIndex(
                                count($columns) - 1
                            ) .
                            $this->getPageActiveRow()
                        )
                            ->getFont()
                            ->setBold(true);
                    }
                    $this->incrementPageActiveRow();
                }

                // Header row
                $activeCol = 0;
                foreach ($columns as $column) {
                    $page
                        ->setCellValue(
                            PHPExcel_Cell::stringFromColumnIndex(
                                $activeCol
                            ) . $this->getPageActiveRow(),
                            $column['label']
                        );
                    $activeCol++;
                }

                if (ArrayHelper::getValue($headerOptions, 'bold', true)) {
                    $page->getStyle(
                        PHPExcel_Cell::stringFromColumnIndex(0) .
                        $this->getPageActiveRow() .
                        ':' .
                        PHPExcel_Cell::stringFromColumnIndex(
                            count($columns) - 1
                        ) .
                        $this->getPageActiveRow()
                    )
                        ->getFont()
                        ->setBold(true);
                }

                $this->incrementPageActiveRow();


//                Yii::trace('test test test');
//                Yii::trace('$dataProvider->pagination->pageCount ? ' . VarDumper::dumpAsString($dataProvider->pagination->pageCount));
//                for ($page = 0; $page < $dataProvider->pagination->pageCount; $page++) {
//                    Yii::trace('???');
//                    Yii::trace($dataProvider->pagination->pageCount);
//                    $dataProvider->pagination->setPage($page);
//                    $models = $dataProvider->getModels();
//
//                    foreach ($models as &$model) {
//                        $activeCol = 0;
//                        foreach ($columns as $column) {
//                            if ($column['content'] instanceof Closure) {
//                                $cellValue = call_user_func(
//                                    $column['content'],
//                                    $model,
//                                    $activeCol,
//                                    $this->getPageActiveRow(),
//                                    $this
//                                );
//                            } else {
//                                $cellValue = $column['content'];
//                            }
//                            if ($stripTags) {
//                                $cellValue = strip_tags($cellValue);
//                            }
//                            $page
//                                ->setCellValue(
//                                    PHPExcel_Cell::stringFromColumnIndex(
//                                        $activeCol
//                                    ) . $this->getPageActiveRow(),
//                                    $cellValue
//                                );
//                            $activeCol++;
////                            $column = null;
////                            gc_collect_cycles();
//                        }
//                        $this->incrementPageActiveRow();
////                        $model = null;
//                    }
//                }
                // Data rows
                foreach ($models as &$model) {
                    $activeCol = 0;
                    foreach ($columns as $column) {
                        if ($column['content'] instanceof Closure) {
                            $cellValue = call_user_func(
                                $column['content'],
                                $model,
                                $activeCol,
                                $this->getPageActiveRow(),
                                $this
                            );
                        } else {
                            $cellValue = $column['content'];
                        }
                        if ($stripTags) {
                            $cellValue = strip_tags($cellValue);
                        }
                        $page
                            ->setCellValue(
                                PHPExcel_Cell::stringFromColumnIndex(
                                    $activeCol
                                ) . $this->getPageActiveRow(),
                                $cellValue
                            );
                        $activeCol++;
                        $column = null;
//                        gc_collect_cycles();
                    }
                    $this->incrementPageActiveRow();
                    $model = null;
                }
//                $after = memory_get_usage(true);
//                Yii::trace('Memory usage after: ' . VarDumper::dumpAsString($after));
                return true;
            } catch (\PHPExcel_Exception $e) {
                Yii::error('PHPExcel Exception: ' . $e->getMessage());
                return false;
            }
        }
        return null;
    }

    /**
     * @param $filename
     *
     * @throws \PHPExcel_Reader_Exception
     * @throws \yii\base\ExitException
     */
    public function download(
        $filename
    ) {
        $objWriter = PHPExcel_IOFactory::createWriter(
            $this->objPHPExcel,
            'Excel5'
        );

        header('Content-type: application/vnd.ms-excel');
        header(
            'Content-Disposition: attachment; filename="' . Inflector::slug(
                $filename
            ) . '.xls' . '"'
        );
        header('Cache-Control: max-age=0');
        $objWriter->save('php://output');

        Yii::$app->end();
    }
}