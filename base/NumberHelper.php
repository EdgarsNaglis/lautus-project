<?php
namespace app\base;

/**
 * Class NumberHelper
 */
class NumberHelper
{
    public static function isInteger($var)
    {
        if (is_int($var)) {
            return true;
        } elseif (is_numeric($var)) {
            if (extension_loaded('gmp')) {
                $gmpNumber = gmp_init($var);
                return (\gmp_cmp($gmpNumber, PHP_INT_MAX) > 0);
            } elseif (extension_loaded('bcmath')) {
                return (\bccomp($var, PHP_INT_MAX, 0) !== 1);
            } else {
                return (
                    strlen((string) $var) <= strlen((string) PHP_INT_MAX) &&
                    floor($var) == $var
                );
            }
        }
        return false;
    }
}
