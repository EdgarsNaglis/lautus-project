<?php
namespace app\base;

use yii\web\User;

/**
 * Class WebUser
 *
 * @property WebUserIdentity|null $identity The identity object associated with the currently logged-in
 * user. `null` is returned if the user is not logged in (not authenticated).
 *
 * @package app\base
 */
class WebUser extends User {

}