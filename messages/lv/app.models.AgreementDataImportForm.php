<?php
return [
    'Import file' => 'Ielādēt failu',
    'Mode' => 'Režījums',
    'Sheet type' => 'Darba lapas tips',
    'Sheet number' => 'Darba lapas numurs',
];
