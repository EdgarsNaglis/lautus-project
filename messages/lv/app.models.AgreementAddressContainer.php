<?php
return [
    'Agreement address container ID' => 'Konteinera adreses ID',
    'Agreement address ID' => 'Līguma adreses ID',
    'Agreement number' => 'CSA Līgums (Līguma rinda)',
    'Agreement description' => 'Līguma rinda',
    'Waste collection address' => 'Objekts',
    'Waste container address' => 'Iela (Objekts)',
    'Building address' => 'Mājas Nr. (Objekts)',
    'Waste container type' => 'Konteineris',
    'Waste container number' => 'Konteinera Nr.',
    'Waste container count' => 'Faktisks daudzums',
    'Company' => 'Uzņēmums',
    'Client name' => 'Kontaktpersona',

    'Waste container table' => 'Atkritumu konteineru tabula',
];
