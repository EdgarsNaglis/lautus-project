<?php

return [
    'Simulate import, show as table' => 'Ielādēto failu parādīt kā tabulu',
    'Import in DB' => 'Ielādēt failu datubāzē',
    'Import' => 'Importēt',
    'Export (action)' => 'Eksportēt',
    'Export' => 'Eksports',
    'Login' => 'Pieslēgties',
    'Logout' => 'Atslēgties',
    'Loading data' => 'Notiek datu ielāde',
    'Agreement Address' => 'Līgumu adreses',
    'Agreement Address Containers' => 'Līgumu adreses konteineri',
    'Create Agreement Address Container' => 'Pievienot jaunu konteineri adreisei',
    'Create Agreement Address' => 'Pievienot jaunu adresi',
    'Agreement Addresses' => 'Līgumu adreses',
    'Please fill out the following fields to login:' => 'Lūdzu aizpildiet pieslēgšanās formu:',

];
