<?php
return [
    'Agreement address ID' => 'Līguma adreses ID',
    'Agreement number' => 'CSA Līgums',
    'Agreement date' => 'Līguma datums (CSA Līgums)',
    'Waste collection start date' => 'Izvešanu sākuma datums',
    'Waste collection end date or agreement end date' => 'Izvešanu beigu datums',
    'Registration code' => 'Reģ. Nr. (Maksātājs (uzņēmums))',
    'Client code' => 'Personas Kods (Maksātājs (kontaktpersona))',
    'Company e-mail address' => 'E-pasts (Maksātājs (uzņēmums))',
    'Client e-mail address' => 'E-pasts (Maksātājs (kontaktpersona))',
    'Waste collection address' => 'Objekts',
    'Waste container address' => 'Iela (Objekts)',
    'Building address' => 'Mājas Nr. (Objekts)',
    'Waste collection frequency' => 'Atkārtošanas nosacījums',

    'Waste address table' => 'Atkritumu adrešu tabula',
    'Successfully imported {importedCount} entries' => 'Veiksmīgi ielādēti {importedCount} ieraksti',
    'Could not import data from provided file' => 'Nevarēja importēt datus no iesniegtā faila',

];
