<?php

return [
    'Search' => 'Meklēt',
    'Filter' => 'Atlasīt',
    'Apply' => 'Izmantot',
    'Reset' => 'Atiestatīt',

   'Import' => 'Ielādēt datus',
   'Agreement' => 'Līgumu adreses',
   'Containers' => 'Līgumu adreses konteineri',
   'Login' => 'Pieslēgties sistēmai',
];
