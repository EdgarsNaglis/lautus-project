<?php
namespace app\controllers;

use Yii;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use app\base\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use yii\helpers\Url;
use yii\web\Response;

/**
 * AdminController implements admin user login and dashboard capabilities
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\Admin
 */
class AdminController extends Controller
{

    public $layout = 'admin';

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['index', 'logout'],
                'rules' => [
                    [
                        'actions' => ['index', 'logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * @return string|Response
     */
    public function actionLogin()
    {
        Yii::$app->homeUrl = Url::to(['admin/index']);
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        if (strlen(Yii::$app->user->returnUrl) > 0) {
            Yii::$app->getSession()->setFlash(
                'info',
                Yii::t(
                    'app',
                    'After successfull login, you will be redirected back to {returnUrl}',
                    [
                        'returnUrl' => Html::a(Yii::$app->user->returnUrl, Yii::$app->user->returnUrl)
                    ]
                )
            );
        }
        $this->heading = Yii::t('app', 'Login');
        $this->view->title = $this->heading . ' | '.Yii::$app->name;
        $this->view->params['breadcrumbs'][] = '<i class="fa fa-sign-in"></i> ' . $this->heading;
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
