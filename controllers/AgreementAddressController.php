<?php

namespace app\controllers;

use app\models\AgreementAddressContainer;
use app\models\AgreementDataImportForm;
use app\models\AgreementDataImportFormTest;
use DateTime;
use DateTimeZone;
use Yii;
use app\models\AgreementAddress;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\VarDumper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * AgreementAddressController implements the CRUD actions for AgreementAddress model.
 */
class AgreementAddressController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['index', 'view', 'create', 'update', 'delete', 'import'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete', 'import'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all AgreementAddress models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => AgreementAddress::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single AgreementAddress model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new AgreementAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new AgreementAddress();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing AgreementAddress model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing AgreementAddress model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the AgreementAddress model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return AgreementAddress the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = AgreementAddress::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    /**
     * Creates a new AgreementAddress model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionImport()
    {
        $renderParams = [
            'addressesData' => null,
            'containersData' => null,
        ];
        $model = new AgreementDataImportForm();
        if ($model->load(Yii::$app->request->post())) {
            $model->importFile = UploadedFile::getInstance($model, 'import_file');
            $model->import_file = $model->importFile->baseName;
            if ($model->import()) {
                if ($model->getIsSimulateMode()) {
                    if ($model->getIsSheetAgreementAddress()) {
                        $renderParams['addressesData'] = $model->addressesData;
                    }
                    if ($model->getIsSheetAgreementAddressContainer()) {
                        $renderParams['containersData'] = $model->containersData;
                    }
                } else {
                    // @todo Log imported row info
                    Yii::$app->session->addFlash(
                        'success',
                        Yii::t('app.models.AgreementAddress',
                            'Successfully imported {importedCount} entries',
                            [
                                'importedCount' => $model->getImportedCount()
                            ])
                    );
                    return $this->refresh();
                }
            } else {
                if ($model->hasErrors('importFile')) {
                    $model->addErrors([
                        'import_file' => $model->getErrors('importFile')
                    ]);
                }
                Yii::$app->session->addFlash(
                    'error',
                    Yii::t('app.models.AgreementAddress', 'Could not import data from provided file')
                );
                Yii::error(
                    'Could import with AgreementDataImportForm: ' .
                    VarDumper::dumpAsString($model->errors)
                );
            }
        }
        $renderParams['model'] = $model;

        return $this->render('import', $renderParams);
    }

//    /**
//     * Creates a new AgreementAddress model.
//     * If creation is successful, the browser will be redirected to the 'view' page.
//     * @return mixed
//     */
//    public function actionImportTest()
//    {
//        $renderParams = [
//            'addressesData' => null,
//            'containersData' => null,
//        ];
//        $model = new AgreementDataImportFormTest();
//        if ($model->load(Yii::$app->request->post())) {
//            $model->importFile = UploadedFile::getInstance($model, 'import_file');
//            $model->import_file = $model->importFile->baseName;
//
//
//            if ($model->import()) {
//                if ($model->getIsSimulateMode()) {
//                    $renderParams['addressesData'] = $model->addressesData;
//                    $renderParams['containersData'] = $model->containersData;
//                } else {
//                    // @todo Log imported row info
//                    Yii::$app->session->addFlash(
//                        'success',
//                        Yii::t('app.models.AgreementAddress',
//                            'Successfully imported {importedCount} entries',
//                            [
//                                'importedCount' => $model->getImportedCount()
//                            ])
//                    );
//                    return $this->refresh();
//                }
//            } else {
//                if ($model->hasErrors('importFile')) {
//                    $model->addErrors([
//                        'import_file' => $model->getErrors('importFile')
//                    ]);
//                }
//                Yii::$app->session->addFlash(
//                    'error',
//                    Yii::t('app.models.AgreementAddress', 'Could not import data from provided file')
//                );
//                Yii::error(
//                    'Could import with AgreementDataImportForm: ' .
//                    VarDumper::dumpAsString($model->errors)
//                );
//            }
//        }
//        $renderParams['model'] = $model;
//
//        return $this->render('import-test', $renderParams);
//    }


    public function actionExportDemo()
    {
        return $this->render('export-demo');
    }

    public function actionExport()
    {
        $returnData = [
            'addresses' => [],
            'containers' => []
        ];

        $agreementAddressesQuery = AgreementAddress
            ::find()
            ->orderBy([
                'id' => SORT_DESC
            ])
//            ->where(['agreement_number'])
            ->asArray();

        $agreementAddressesContainerQuery = AgreementAddressContainer
            ::find()
            ->orderBy([
                'id' => SORT_DESC
            ])
//            ->where(['agreement_number'])
            ->asArray();

        $agreementAddresses = $agreementAddressesQuery->all();
        $agreementAddressesContainers = $agreementAddressesContainerQuery->all();

        if (!empty($agreementAddresses)) {
            $attributeMap = [
                'id' => 'ID',
                'agreement_number' => 'agreementNumber',
                'agreement_date' => 'agreementDate',
                'waste_collection_start_date' => 'wasteCollectionStartDate',
                'waste_collection_end_date' => 'wasteCollectionEndDate',
                'registration_code' => 'registrationCode',
                'client_code' => 'clientCode',
                'company_email' => 'companyEmail',
                'client_email' => 'clientEmail',
                'waste_collection_address' => 'wasteCollectionAddress',
                'waste_container_address' => 'wasteContainerAddress',
                'building_address' => 'buildingAddress',
                'waste_collection_frequency' => 'wasteCollectionFrequency',
            ];
            foreach ($agreementAddresses as $agreementAddress) {
                $data = [];
                foreach ($attributeMap as $attribute => $outputKey) {
                    $value = $agreementAddress[$attribute];
                    if ($outputKey === 'agreementDate' ||
                        $outputKey === 'wasteCollectionStartDate' ||
                        $outputKey === 'wasteCollectionEndDate') {
                        if (!empty($value)) {
                            $date = DateTime::createFromFormat('Y-m-d H:i:s', $value);
                            $value = $date->format('Y-m-d');
                        }
                    }

                    $data[$outputKey] = $value;
                }
                $returnData['addresses'][] = $data;
            }
        }

        if (!empty($agreementAddressesContainers)) {
            $attributeMap = [
                'id' => 'ID',
//                    'agreement_address_id' =>'Agreement address ID',
                'agreement_number' => 'agreementNumber',
                'agreement_description' => 'agreementDescription',
                'waste_collection_address' => 'wasteCollectionAddress',
                'waste_container_address' => 'wasteContainerAddress',
                'building_address' => 'buildingAddress',
                'waste_container_type' => 'wasteContainerType',
                'waste_container_number' => 'wasteContainerNumber',
                'waste_container_count' => 'wasteContainerCount',
                'company' => 'company',
                'client_name' => 'clientName',
            ];
            foreach ($agreementAddressesContainers as $agreementAddressesContainer) {
                $data = [];
                foreach ($attributeMap as $attribute => $outputKey) {
                    $value = $agreementAddressesContainer[$attribute];
                    $data[$outputKey] = $value;
                }
                $returnData['containers'][] = $data;
            }
        }

        Yii::$app->response->format = Response::FORMAT_JSON;
        return $returnData;
    }
}
