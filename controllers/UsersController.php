<?php
namespace app\controllers;

use Yii;
use app\models\User;
use app\models\search\User as UserSearch;
use app\base\Controller;
use yii\helpers\Url;
use yii\web\ConflictHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;

/**
 * UsersController implements the CRUD actions for {@link User} model.
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\models\User
 */
class UsersController extends Controller
{

    public $layout = 'admin';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only'  => ['index', 'view', 'create', 'update', 'delete'],
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'create', 'update', 'delete'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::class,
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public static function getIcon()
    {
        return '<i class="fa fa-users"></i>';
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $searchModel->scenario = 'search';
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->heading = Yii::t('app.models.User', 'Users');
        $this->view->title = $this->heading . ' | ' . Yii::$app->name;
        $this->view->params['breadcrumbs'][] = $this->getIcon() . ' ' . $this->heading;

        return $this->render('index', [
            'searchModel'  => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function addParentBreadcrumbs()
    {
        $this->view->params['breadcrumbs'][] = [
            'label' => $this->getIcon() . ' ' . Yii::t('app.models.User', 'Users'),
            'url'   => ['index'],
        ];
    }

    /**
     * Displays a single User model.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $this->heading = $model->id . '. &quot;' . $model->title . '&quot;';
        $this->subHeading = Yii::t('app.models.User', 'User');
        $this->view->title = $this->subHeading . ' ' . $this->heading . ' | ' . Yii::$app->name;
        $this->addParentBreadcrumbs();
        $this->view->params['breadcrumbs'][] = $this->heading;

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     *
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $mode = Yii::$app->request->post('mode', 'save');
            if ($mode == 'save-and-create-new') {
                Yii::$app->session->addFlash(
                    'success',
                    Yii::t('app', 'New <a href="{url}">{modelClass}</a> successfully created', [
                        'modelClass' => Yii::t('app.models.User', 'User'),
                        'url'        => Url::to(['view', 'id' => $model->id])
                    ])
                );

                return $this->redirect(['create']);
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $model->loadDefaultValues();
            $this->heading = Yii::t('app', 'Create new {modelClass}', [
                'modelClass' => Yii::t('app.models.User', 'User'),
            ]);
            $this->view->title = $this->heading . ' | ' . Yii::$app->name;
            $this->addParentBreadcrumbs();
            $this->view->params['breadcrumbs'][] = Yii::t('app', 'Create new');

            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param integer $id
     *
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $mode = Yii::$app->request->post('mode', 'save');
            if ($mode == 'save-and-create-new') {
                Yii::$app->session->addFlash(
                    'success',
                    Yii::t('app', 'Save successfull')
                );

                return $this->redirect(['create']);
            } else {
                return $this->redirect(['view', 'id' => $model->id]);
            }
        } else {
            $this->heading = Yii::t('app', '{id}. &quot;{title}&quot;', [
                'id'    => $model->id,
                'title' => $model->title
            ]);
            $this->subHeading = Yii::t('app.models.User', 'User');
            $this->view->title = Yii::t('app', 'Updating {modelClass} {id}. {title} | {appName}', [
                'modelClass' => $this->subHeading,
                'id'         => $model->id,
                'title'      => $model->title,
                'appName'    => Yii::$app->name,
            ]);
            $this->addParentBreadcrumbs();
            $this->view->params['breadcrumbs'][] = [
                'label' => $model->id . '. &quot;' . $model->title . '&quot;',
                'url'   => ['view', 'id' => $model->id],
            ];
            $this->view->params['breadcrumbs'][] = Yii::t('app', 'Update');

            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws ConflictHttpException
     * @throws NotFoundHttpException
     * @throws \Exception
     */
    public function actionDelete($id)
    {
        $user = $this->findModel($id);

        // Do not let delete last admin user
        if ($user->hasIsAdminStatus && User::countUsersWithStatus(User::STATUS_IS_ADMIN) == 1) {
            Yii::$app->session->addFlash(
                'warning',
                Yii::t(
                    'app.models.User',
                    'To avoid lockout out of admin interface, last admin user ({username}) deletion was interrupted',
                    [
                        'username' => $user->username
                    ]
                )
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }

        // Do not let delete yourself
        elseif ($user->id == Yii::$app->user->id) {
            Yii::$app->session->addFlash(
                'warning',
                Yii::t(
                    'app.models.User',
                    'To avoid lockout out of admin interface, deletion of your own user account ({username}) was interrupted'
                )
            );

            return $this->redirect($_SERVER['HTTP_REFERER']);
        }
        $user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested User does not exist.');
        }
    }
}
