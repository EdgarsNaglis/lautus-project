<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\AgreementAddress */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Agreement Addresses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="agreement-address-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'agreement_number',
            'agreement_date',
            'waste_collection_start_date',
            'waste_collection_end_date',
            'client_code',
            'client_email:email',
            'waste_collection_address',
            'waste_container_address',
            'building_address',
            'waste_collection_frequency',
        ],
    ]) ?>

</div>
