<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Agreement Addresses';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="agreement-address-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Agreement Address', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'agreement_number',
            'agreement_date',
            'waste_collection_start_date',
            'waste_collection_end_date',
            //'client_code',
            //'client_email:email',
            //'waste_collection_address',
            //'waste_container_address',
            //'building_address',
            //'waste_collection_frequency',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
