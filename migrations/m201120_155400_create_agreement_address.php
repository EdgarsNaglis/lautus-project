<?php
/**
 * Contains \app\migrations\m201120_155400_create_agreement_address
 * @noinspection PhpUnused
 */

namespace app\migrations;

use deele\devkit\db\SchemaHelper;
use yii\db\Migration;

/**
 * Class m201120_155400_create_agreement_address "agreement_address" table
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\models\AgreementAdress
 */
class m201120_155400_create_agreement_address extends Migration
{

    public $tableName = 'agreement_address';

    /**
     * Creates table
     *
     * @return bool
     */
    public function up()
    {
        if (!SchemaHelper::tablesExist($this->tableName)) {
            return $this->createMainTable();
        }

        return false;
    }

    /**
     * Drops tables
     */
    public function down()
    {
        if (SchemaHelper::tablesExist($this->tableName)) {
            return $this->deleteMainTable();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function createMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);
        $this->createTable(
            $tableName,
            [
                'id' => $this->primaryKey()
                    ->comment('Agreement address ID'),
                'agreement_number' => $this->string(50)
                    ->comment('Agreement number'),
                'agreement_date' => $this->dateTime()
                    ->comment('Agreement date'),
                'waste_collection_start_date' => $this->dateTime()
                    ->comment('Waste collection start date'),
                'waste_collection_end_date' => $this->dateTime()
                    ->comment('Waste collection end date or agreement end date'),
                'registration_code' => $this->string(255)
                    ->comment('Registration code'),
                'client_code' => $this->string(255)
                    ->comment('Client code or registration code'),
                'company_email' => $this->string(255)
                    ->comment('Company e-mail address'),
                'client_email' => $this->string(255)
                    ->comment('Client e-mail address'),
                'waste_collection_address' => $this->string(255)
                    ->comment('Waste collection address'),
                'waste_container_address' => $this->string(255)
                    ->comment('Waste container address'),
                'building_address' => $this->string(255)
                    ->comment('Building address'),
                'waste_collection_frequency' => $this->string(255)
                    ->comment('Waste collection frequency'),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
        );

        return true;
    }

    /**
     * @return bool
     */
    public function deleteMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);

        $this->dropTable($tableName);

        return true;
    }
}
