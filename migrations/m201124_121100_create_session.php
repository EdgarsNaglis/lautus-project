<?php
/**
 * Contains \app\migrations\m201124_121100_create_session
 * @noinspection PhpUnused
 */

namespace app\migrations;

use deele\devkit\db\SchemaHelper;
use yii\db\Migration;

/**
 * Class m201124_121100_create_session "session" table
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 */
class m201124_121100_create_session extends Migration
{

    public $tableName = 'session';

    /**
     * Creates table
     *
     * @return bool
     */
    public function up()
    {
        if (!SchemaHelper::tablesExist($this->tableName)) {
            return $this->createMainTable();
        }

        return false;
    }

    /**
     * Drops tables
     */
    public function down()
    {
        if (SchemaHelper::tablesExist($this->tableName)) {
            return $this->deleteMainTable();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function createMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);
        $this->createTable(
            $tableName,
            [
                'id' => $this->string(60)
                    ->notNull(),
                'expire' => $this->integer()
                    ->comment('Expire'),
                'data' => $this->binary(65536)
                    ->comment('Data'),
                'PRIMARY KEY (`id`)'
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
        );

        return true;
    }

    /**
     * @return bool
     */
    public function deleteMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);

        $this->dropTable($tableName);

        return true;
    }
}
