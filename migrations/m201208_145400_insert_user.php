<?php
/**
 * Contains \app\migrations\m201208_145400_insert_user
 * @noinspection PhpUnused
 */

namespace app\migrations;

use app\models\User;
use deele\devkit\db\SchemaHelper;
use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m201208_145400_insert_user insert new user in "user" table
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\models\User
 */
class m201208_145400_insert_user extends Migration
{

    public $tableName = 'user';

    /**
     * Creates table
     *
     * @return bool
     */
    public function up()
    {
        if (SchemaHelper::tablesExist($this->tableName)) {
            $tableName = SchemaHelper::prefixedTable($this->tableName);

            $this->insert(
                $tableName,
                [
                    'id' => 2,
                    'username' => 'martins.zvigulis',
                    'password_hash' => User::generatePasswordHash('Lautus$Project$Martins$Zvigulis'),
                    'status' => 1,
                    'created_at' => new Expression('UTC_TIMESTAMP()'),
                    'created_by' => 1,
                    'updated_at' => new Expression('UTC_TIMESTAMP()'),
                    'updated_by' => 1
                ]);
                return true;
        }

        return false;
    }

    /**
     * Drops tables
     */
    public function down()
    {
        if (SchemaHelper::tablesExist($this->tableName)) {
            $tableName = SchemaHelper::prefixedTable($this->tableName);

            $this->delete(
                $tableName,
                [
                    'id' => 2,
                    'username' => 'martins.zvigulis',
                    'password_hash' => User::generatePasswordHash('Lautus$Project$Martins$Zvigulis'),
                    'status' => 1,
                    'created_at' => new Expression('UTC_TIMESTAMP()'),
                    'created_by' => 1,
                    'updated_at' => new Expression('UTC_TIMESTAMP()'),
                    'updated_by' => 1
                ]);
            return true;
        }

        return false;
    }


}
