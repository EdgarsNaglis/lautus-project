<?php
/**
 * Contains \app\migrations\m201124_091900_create_user
 * @noinspection PhpUnused
 */

namespace app\migrations;

use app\models\User;
use deele\devkit\db\SchemaHelper;
use yii\db\Expression;
use yii\db\Migration;

/**
 * Class m201124_091900_create_user "user" table
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\models\User
 */
class m201124_091900_create_user extends Migration
{

    public $tableName = 'user';

    /**
     * Creates table
     *
     * @return bool
     */
    public function up()
    {
        if (!SchemaHelper::tablesExist($this->tableName)) {
            if ($this->createMainTable()) {
                $this->insert($this->tableName, [
                    'id' => 1,
                    'username' => 'admin',
                    'password_hash' => User::generatePasswordHash('admin'),
                    'status' => 1,
                    'created_at' => new Expression('UTC_TIMESTAMP()'),
                    'created_by' => 1,
                    'updated_at' => new Expression('UTC_TIMESTAMP()'),
                    'updated_by' => 1
                ]);
                return true;
            }
        }

        return false;
    }

    /**
     * Drops tables
     */
    public function down()
    {
        if (SchemaHelper::tablesExist($this->tableName)) {
            return $this->deleteMainTable();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function createMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);
        $this->createTable(
            $tableName,
            [
                'id' => $this->primaryKey()
                    ->comment('User ID'),
                'username' => $this->string(100)
                    ->notNull()
                    ->comment('Username'),
                'password_hash' => $this->string(100)
                    ->notNull()
                    ->comment('Password hash'),
                'status' => $this->smallInteger()
                    ->notNull()
                    ->defaultValue(1)
                    ->comment('Status'),
                'created_at' => $this->dateTime()
                    ->notNull()
                    ->comment('Created at'),
                'created_by' => $this->integer()
                    ->notNull()
                    ->comment('Creator user ID'),
                'updated_at' => $this->dateTime()
                    ->comment('Updated at'),
                'updated_by' => $this->integer()
                    ->comment('Updater user ID'),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
        );

        // username
        $this->createIndex(
            SchemaHelper::createIndexName('username'),
            $tableName,
            'username'
        );

        // status
        $this->createIndex(
            SchemaHelper::createIndexName('status'),
            $tableName,
            'status'
        );

        // created_by
        $this->addForeignKey(
            SchemaHelper::createForeignKeyName($this->tableName, 'created_by'),
            $tableName,
            'created_by',
            'user',
            'id',
            SchemaHelper::createForeignKeyType(SchemaHelper::FK_NO_ACTION),
            SchemaHelper::createForeignKeyType(SchemaHelper::FK_CASCADE)
        );

        // updated_by
        $this->addForeignKey(
            SchemaHelper::createForeignKeyName($this->tableName, 'updated_by'),
            $tableName,
            'updated_by',
            'user',
            'id',
            SchemaHelper::createForeignKeyType(SchemaHelper::FK_NO_ACTION),
            SchemaHelper::createForeignKeyType(SchemaHelper::FK_CASCADE)
        );


        return true;
    }

    /**
     * @return bool
     */
    public function deleteMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);

        // username
        $this->dropIndex(
            SchemaHelper::createIndexName('username'),
            $tableName
        );

        // status
        $this->dropIndex(
            SchemaHelper::createIndexName('status'),
            $tableName
        );

        // created_by
        $this->dropForeignKey(
            SchemaHelper::createForeignKeyName($this->tableName, 'created_by'),
            $tableName
        );

        // updated_by
        $this->dropForeignKey(
            SchemaHelper::createForeignKeyName($this->tableName, 'updated_by'),
            $tableName
        );

        $this->dropTable($tableName);

        return true;
    }
}
