<?php
/**
 * Contains \app\migrations\m201123_101400_create_agreement_address__container
 * @noinspection PhpUnused
 */

namespace app\migrations;

use deele\devkit\db\SchemaHelper;
use yii\db\Migration;

/**
 * Class m201123_101400_create_agreement_address__container "agreement_address__container" table
 *
 * @author Edgars Naglis <edgarsnaglis300@gmail.com>
 *
 * @copyright 2020 SIA "Web Multishop Company"
 *
 * @package app\models\AgreementAddressContainer
 */
class m201123_101400_create_agreement_address__container extends Migration
{

    public $tableName = 'agreement_address__container';

    /**
     * Creates table
     *
     * @return bool
     */
    public function up()
    {
        if (!SchemaHelper::tablesExist($this->tableName)) {
            return $this->createMainTable();
        }

        return false;
    }

    /**
     * Drops tables
     */
    public function down()
    {
        if (SchemaHelper::tablesExist($this->tableName)) {
            return $this->deleteMainTable();
        }

        return false;
    }

    /**
     * @return bool
     */
    public function createMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);
        $this->createTable(
            $tableName,
            [
                'id' => $this->primaryKey()
                    ->comment('Agreement address container ID'),
                'agreement_address_id' => $this->integer()
                    ->comment('Agreement address ID'),
                'agreement_number' => $this->string(50)
                    ->comment('Agreement number'),
                'agreement_description' => $this->string(255)
                    ->comment('Agreement description'),
                'waste_collection_address' => $this->string(255)
                    ->comment('Waste collection address'),
                'waste_container_address' => $this->string(255)
                    ->comment('Waste container address'),
                'building_address' => $this->string(255)
                    ->comment('Building address'),
                'waste_container_type' => $this->string(255)
                    ->comment('Waste container type'),
                'waste_container_number' => $this->string(255)
                    ->comment('Waste container number'),
                'waste_container_count' => $this->integer()
                    ->comment('Waste container count'),
                'company' => $this->string(255)
                    ->comment('Company'),
                'client_name' => $this->string(255)
                    ->comment('Client name'),
            ],
            'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB'
        );

        // agreement_address_id
        $this->createIndex(
            SchemaHelper::createIndexName('agreement_address_id'),
            $tableName,
            'agreement_address_id'
        );
        $this->addForeignKey(
            SchemaHelper::createForeignKeyName($this->tableName, 'agreement_address_id'),
            $tableName,
            'agreement_address_id',
            'agreement_address',
            'id',
            SchemaHelper::createForeignKeyType(SchemaHelper::FK_CASCADE),
            SchemaHelper::createForeignKeyType(SchemaHelper::FK_CASCADE)
        );

        return true;
    }

    /**
     * @return bool
     */
    public function deleteMainTable()
    {
        $tableName = SchemaHelper::prefixedTable($this->tableName);

        // agreement_address_id
        $this->dropForeignKey(
            SchemaHelper::createForeignKeyName($this->tableName, 'agreement_address_id'),
            $tableName
        );
        $this->dropIndex(
            SchemaHelper::createIndexName('agreement_address_id'),
            $tableName
        );

        $this->dropTable($tableName);

        return true;
    }
}
